<?php wp_footer(); ?>

<div class="rodape-total">
    
	<div class="rodape">


		<div class="rodape-logo">
			<img src="<?php bloginfo('template_directory'); ?>/pw-images/logo-Branco.png" alt="" title="" />
			<p>AFLUX BRASIL LTDA</p>
		</div>

		<div class="rodape-contato">

			<div class="rodape-contato-conteudo">
				<i class="fas fa-phone"></i>
				<p>Telefone <br> +55 11 2376-9961 </p>
			</div>

			<div class="rodape-contato-conteudo">
				<i class="fas fa-mobile-alt"></i>
				<p>Celular: <br> +55 11 98987-9151 <br> +55 11 98983-5189 </p>
			</div>

			<div class="rodape-contato-conteudo">
				<i class="far fa-envelope"></i>
				<p>Emails: <br> comercial@afluxbrasil.com.br </p>
			</div>

			<div class="rodape-contato-conteudo">
				<i class="fas fa-map-marker-alt"></i>
				<p>Endereço <br> Rua Lisboa, 41 - Osvaldo Cruz <br> CEP: 09570-510 - São Caetano do Sul - SP </p>
			</div>
			

		</div>

		<div class="rodape-ProjetoWeb">
		
		<img src="<?php bloginfo('template_directory'); ?>/pw-images/logo-PW-Branco.png" alt="" title="" />

		</div>

	
	</div> <!-- Rodape -->

</div> <!-- Rodape Total -->


	
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-galeria-fancybox/jquery.fancybox.min.js"></script> 
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/swiper.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/javascript.js"></script>
	<script type="text/javascript">
	 
		(function($) {	
			'use strict';
			//https://github.com/jlmakes/scrollreveal.js
			//[data-sr="enter direction"]
			//direction = top, bottom, right, left	
			window.sr = ScrollReveal();
			sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
		})();
		var swiper = new Swiper('.swiper-container', {
			pagination: '',
			slidesPerView: 1,
			slidesPerColumn: 1,
			paginationClickable: true,
			spaceBetween: 0,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			freeMode: false
    	});
		  
		  
	</script>
</body>
</html>