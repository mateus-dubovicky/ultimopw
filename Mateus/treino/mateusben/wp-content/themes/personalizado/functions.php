<?php


function redirect_user_to( $redirect_to, $user ) {
	global $user;

	if ( $user->user_level < 10 ) {
		$redirect_to = get_option('siteurl');

	} else {
		$redirect_to = get_option('siteurl'). '/wp-admin/';
	}

	return $redirect_to;
}

add_filter( 'login_redirect', 'redirect_user_to', 10, 3 );

function cuar_change_default_customer_page( $redirect_to ) {
  // customer-private-files is the slug where we want the user to be redirected to
  return 'customer-private-files';
}
// customer-home is the slug of the redirect page we want to change
add_filter( 'cuar/routing/redirect/root-page-to-slug?slug=' . 'customer-home', 'cuar_change_default_customer_page' );

add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
function baw_hack_wp_title_for_home( $title )
{
  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
    return __( '', 'theme_domain' ) . '' . get_bloginfo( 'title' );
  }
  return $title;
}

add_action( 'init', 'my_add_excerpts_to_pages' );

function my_add_excerpts_to_pages() {

	 add_theme_support('post-thumbnails');
     add_post_type_support( 'page', 'excerpt' );
     add_post_type_support( 'planos', 'thumbnail' );
     add_post_type_support( 'planos', 'excerpt' );

}

if (function_exists('register_sidebar'))
{
    register_sidebar(array(
		'name'			=> 'Sidebar',
        'before_widget'	=> '<div class="widget">',
        'after_widget'	=> '</div>',
		'before_title'	=> '<h3>',
		'after_title'	=> '</h3>',
    ));
}




if ( function_exists( 'register_nav_menu' ) ) {

register_nav_menu( 'menu_topo', 'Este é meu primeiro menu' );

register_nav_menu( 'menu_rodape', 'Este é meu segundo menu' );

}


/****** Add Thumbnails in Manage Posts/Pages List ******/
if ( !function_exists('AddThumbColumn') && function_exists('add_theme_support') ) {

    // for post and page
    add_theme_support('post-thumbnails');

    function AddThumbColumn($cols) {

        $cols['thumbnail'] = __('Thumbnail');

        return $cols;
    }

    function AddThumbValue($column_name, $post_id) {

            $width = (int) 60;
            $height = (int) 60;

            if ( 'thumbnail' == $column_name ) {
                // thumbnail of WP 2.9
                $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
                // image from gallery
                $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
                if ($thumbnail_id)
                    $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
                elseif ($attachments) {
                    foreach ( $attachments as $attachment_id => $attachment ) {
                        $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
                    }
                }
                    if ( isset($thumb) && $thumb ) {
                        echo $thumb;
                    } else {
                        echo __('None');
                    }
            }
    }

	    // for posts
	    add_filter( 'manage_posts_columns', 'AddThumbColumn' );
	    add_action( 'manage_posts_custom_column', 'AddThumbValue', 10, 2 );

	    // for pages
	    add_filter( 'manage_pages_columns', 'AddThumbColumn' );
	    add_action( 'manage_pages_custom_column', 'AddThumbValue', 10, 2 );
	}

?>