<?php
$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 12";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 12\" />";
$dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">

	<div class="titulo">
		<h1><i class="far fa-building"></i> <br> Pacote a Sua Medida</h1>
	</div>

	<div class="box-conteudo">

		<div class="form contato orc">
			<form action="pw-form" method="post">
				<input name="campo[Nome]" placeholder="Nome:" type="text" />
				<input name="campo[Telefone]" placeholder="Telefone:" type="text" />
				<input name="campo[E-mail]" placeholder="E-mail:" type="text" />
				<select name="campo[Plano]" id="" value="Escolha"> Selecione
					<option value="#">Diário</option>
					<option value="#">Semanal</option>
					<option value="#">Mensal</option>
					<option value="#">Semestral</option>
					<option value="#">Anual</option>
				</select>
				<textarea name="campo[Comentarios]" placeholder="Comentários:"></textarea>
				<input value="Enviar" type="submit" />
			</form>
		</div>

	</div>

</div>