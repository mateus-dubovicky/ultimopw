<?php wp_footer(); ?>

    <div class="rodape-total">
        <div class="rodape">

			<div class="logo-rodape">
				<a href="<?php echo get_site_url(); ?>" title="Titulo">
					<img src="<?php echo get_field('logo_rodape', 13);?>" alt="" title="" />
				</a>
			</div> <!-- Logo Rodape -->
			
			<div class="texto-rodape">
				<?php echo get_field('endereco', 13);?><br />
			</div> <!-- Texto Rodape -->
			
			<div class="texto-rodape">
				<?php echo get_field('telefone', 13);?><br />
				<?php echo get_field('whatsapp', 13);?><br />
				<?php echo get_field('email', 13);?>
			</div> <!-- Texto Rodape -->
            
             
             <div class="logo-pw">
              <a href="https://www.projetowebsite.com.br" title="Projeto Web Site - Agência, Projetos, Marketing, Websites" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/pw-images/logo-pw.png" alt="Projeto Web Site - Agência, Projetos, Marketing, Websites" title="Projeto Web Site - Agência, Projetos, Marketing, Websites" /></a>
            </div>
						
        </div> <!-- Rodape -->
    </div> <!-- Rodape Total -->
	
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-galeria-fancybox/jquery.fancybox.min.js"></script> 
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/swiper.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/javascript.js"></script>
	<script type="text/javascript">
	 
		(function($) {	
			'use strict';
			//https://github.com/jlmakes/scrollreveal.js
			//[data-sr="enter direction"]
			//direction = top, bottom, right, left	
			window.sr = ScrollReveal();
			sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
		})();
		var swiper = new Swiper('.swiper-container', {
			pagination: '',
			slidesPerView: 1,
			slidesPerColumn: 1,
			paginationClickable: true,
			spaceBetween: 0,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			freeMode: false
    	});
		  
		  
	</script>
</body>
</html>