<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <!-- <style><?php echo file_get_contents('template/pw-css/style.css'); ?></style> -->
    <link rel="stylesheet" type="text/css" href="[template]/pw-css/style.css" />
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">

    <!--  GALERIA  -->
    <link rel="stylesheet" href="pw-fancybox/jquery.fancybox.min.css">

    [css]

    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }
    </style>

</head>

<body>

    <div class="topo-total">

        <div class="topo">

            <div class="menu">

                <ul>
                    <li class="logo"><a href="[url]"><img src="[template]/pw-images/logoProffy.png" /></a></li>
                    <li><a href="[url]/galeria-proffy">Galeria</a></li>
                    <li><a href="">Serviços</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contato</a></li>
                </ul>

            </div> <!-- Menu -->

        </div> <!-- Topo -->

    </div> <!-- Topo Total -->



    <div class="global">

        [conteudo]

    </div> <!-- global -->

    <div class="rodape-total">

        <div class="rodape">

            <div class="logo-rodape"><a href="[url]/" title=""><img src="[template]/pw-images/logoProffy.png" alt="" title="" /></a></div> <!-- logo Rodape -->



        </div> <!-- Rodape -->

    </div> <!-- Rodape Total -->

    <script>
        <?php echo file_get_contents('pw-js/jquery.js'); ?>
    </script>
    <script>
        <?php echo file_get_contents('pw-js/swiper.min.js'); ?>
    </script>
    <script>
        <?php echo file_get_contents('pw-js/javascript.js'); ?>
    </script>
    <script>
        <?php echo file_get_contents('pw-js/scrollReveal.js'); ?>
    </script>


    <!--  GALERIA  -->
    <script>
        <?php echo file_get_contents('pw-fancybox/jquery.fancybox.min.js'); ?>
    </script>


    <script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.box-produtos a', {
                duration: 2000,
                origin: 'bottom',
                distance: '100px',
                viewFactor: 0.6
            }, 100);
        })();
    </script>


    <!-- w3c -->
    <script>
        filterSelection("ignite")

        function filterSelection(c) {
            var x, i;
            x = document.getElementsByClassName("filterDiv");
            if (c == "all") c = "";
            // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
            for (i = 0; i < x.length; i++) {
                w3RemoveClass(x[i], "show");
                if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
            }
        }

        // Show filtered elements
        function w3AddClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                if (arr1.indexOf(arr2[i]) == -1) {
                    element.className += " " + arr2[i];
                }
            }
        }

        // Hide elements that are not selected
        function w3RemoveClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                while (arr1.indexOf(arr2[i]) > -1) {
                    arr1.splice(arr1.indexOf(arr2[i]), 1);
                }
            }
            element.className = arr1.join(" ");
        }

        // Add active class to the current control button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }
    </script>


    <!-- w3c -->
</body>

</html>