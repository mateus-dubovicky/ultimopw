<?php
	$dadosPagina["titulo"]   = "Ben - ProjetoWeb";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Pirâmides, Egito, Esculturas Egito, Pirâmide de base, Loja Escultura, Nissan, BMW, Alfa Romeo, Dodge, Audi\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Ben - ProjetoWeb\" />";
	$dadosPagina["css"] = "";
?>



	
<div class="global">

	<div class="box-01">

		<div class="banner-principal">

			<img src="[template]/pw-images/trocaDeMensagens.jpg" alt="Mensagens de Celular" title="Mensagens de celular">

		</div>

	</div>

	<div class="box-02">

		<div class="servico-qualidade">

			<div class="qualidade">
				<i class="fas fa-stopwatch"></i>
				<h2>Agilidade e rápidez</h2>
			</div>

			<div class="qualidade">
			<i class="fas fa-certificate"></i>
				<h2>Preço Competitivo</h2>
			</div>

			<div class="qualidade">
				<i class="fas fa-truck"></i>
				<h2>Frete Grátis</h2>
			</div>

			<div class="qualidade">
				<i class="fas fa-credit-card"></i>
				<h2>Pagamento Parcelado</h2>
			</div>

			<div class="qualidade">
				<i class="fas fa-comment-dots"></i>
				<h2>Resposta Rápida</h2>
			</div>

		</div>

	</div>


	<div class="box-03">

			<div class="tiulo-box-02">
				<h2> ESCOLHA A CATEOGORIA DESEJADA </h2>
			</div>

			<div class="box-imagens">
				<div class="imagem-produto">
					<img src="[template]/pw-images/primeira-piramide.jpg" alt="Piramide Egito" title="Piramide Egito">
					<p>Seneferu</p>
				</div>

				<div class="imagem-produto">
					<img src="[template]/pw-images/segunda-piramide.jpg" alt="Piramide Egito" title="Piramide Egito">
					<p>Quéops</p>
				</div>

				<div class="imagem-produto">
					<img src="[template]/pw-images/terceira-piramide.jpg" alt="Piramide Egito" title="Piramide Egito">
					<p>Tanquerés</p>
				</div>
			</div>


			<div class="tiulo-box-02">
				<h2> ALGUMAS NOVIDADES </h2>
			</div>

			<div class="box-imagens novidades">
				<div class="imagem-produto">
					<img src="[template]/pw-images/primeira-escultura.jpg" alt="Escultura Egito" title="Escultura Egito">
					<p>Tutankhamon</p>
					<button>ORÇAMENTO</button>
					<button class="btnEspiar">  <i class="far fa-eye"></i> Espiar </button>
				</div>

				<div class="imagem-produto">
					<img src="[template]/pw-images/segunda-escultura.jpg" alt="Escultura Egito" title="Escultura Egito">
					<p>Bastet</p>
					<button>ORÇAMENTO</button>
					<button class="btnEspiar">  <i class="far fa-eye"></i> Espiar </button>
				</div>

				<div class="imagem-produto">
					<img src="[template]/pw-images/terceira-escultura.jpg" alt="Escultura Egito" title="Escultura Egito">
					<p>Nefertiti</p>
					<button>ORÇAMENTO</button>
					<button class="btnEspiar">  <i class="far fa-eye"></i> Espiar </button>
				</div>

				<div class="imagem-produto">
					<img src="[template]/pw-images/quarta-escultura-artefato.jpg" alt="Escultura Egito" title="Escultura Egito">
					<p>Papiros</p>
					<button>ORÇAMENTO</button>
					<button class="btnEspiar">  <i class="far fa-eye"></i> Espiar </button>
				</div>

			</div>


			<div class="tiulo-box-02">
				<h2> ALGUNS DOS NOSSOS CLIENTES </h2>
			</div>

			<div class="box-imagens">
					<div class="imagem-produto">
						<img src="[template]/pw-images/logoNissan.png" alt="Nissan" title="Logo Nissan">
					</div>

					<div class="imagem-produto">
						<img src="[template]/pw-images/logoBmw.png" alt="BMW" title="Logo BMW">
					</div>

					<div class="imagem-produto">
						<img src="[template]/pw-images/logoAlfaRomeo.png" alt="Alfa Romeo" title="Logo Alfa Romeo">
					</div>

					<div class="imagem-produto">
						<img src="[template]/pw-images/logoDodge.png" alt="Dodge" title="Logo Dodge">
					</div>

					<div class="imagem-produto">
						<img src="[template]/pw-images/logoAudi.png" alt="Audi" title="Logo Audi">
					</div>

			</div>

	</div>

	<div class="box-final">
		<div class="box-final-conteudo">

			<div class="final-texto">
				<h2>Conheça nossa loja virtual </h2>
				<p>Produtos sem personalização, a pronta entrega e sem quantidade mínima</p>
			</div>

			<div class="box-final-botao">
			<button>Clique para acessar</button>
			</div>
			
		</div>
		
	</div>
    
    
</div> <!-- global -->