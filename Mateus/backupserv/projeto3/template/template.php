<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <!-- <style><?php echo file_get_contents('template/pw-css/style.css');?></style> -->
    <link rel="stylesheet" type="text/css" href="[template]/pw-css/style.css" />
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">  
    
    [css]
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }
    </style>
</head>

<body>

	<div class="topo-total">
    
    	<div class="topo-logotipo">
        
        	<div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logoBen.jpg" alt="" title="" /></a></div> <!-- Logo -->
        
        </div> <!-- Topo -->

        <div class="menu-total">

    
    	<div class="menu">
        
        	<ul>
            	<li><a href="[url]/" title="">HOME</a></li>
                <li><a href="[url]/" title="">SOBRE NÓS</a></li>
                <li><a href="[url]/" title="">PRODUTOS</a></li>
                <li><a href="[url]/" title="">CONTATO</a></li>
            </ul>
        
        </div> <!-- Menu -->

    </div> <!-- menu Total -->
        
        
        <div class="menu-presente">
            

            <div class="presente-icone">
            <i class="fas fa-gift"></i>
            </div>
            
            <div class="presente-conteudo">

            <div class="quantidade-presente">
                <p>1</p>
            </div>

            <p>Item</p>
            </div>

            
            
        </div>
    </div> <!-- Topo Total -->

   
    <div class="topo-pesquisa">

        <input type="text">
            <button class="submitLente" type="submit">  <i class="fa fa-search"></i>  </button>

    </div>

    <div class="linha-menu">
        <hr>
    </div>
    
    

<div class="global">

	[conteudo]
	
</div> <!-- global -->

	<div class="rodape-total">
    
    	<div class="rodape">


            <div class="rodape-logotipo">
        
                <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logoBen.jpg" alt="" title="" /></a></div> <!-- Logo -->
    
            </div> <!-- Rodape Logoipo -->


            <div class="rodape-conteudo">

                <h2>Institucional</h2>
                <p>Home</p>
                <p>Sobre nós</p>
                <p>Produto</p>
                <p>Contato</p>

            </div>

            <div class="rodape-conteudo">

                <h2>AJUDA E SUPORTE</h2>
                <p>Política e Privacidade</p>
                <p>Dúvidas Frequentes</p>
                <p>Termos de Uso</p>
                <p>Política de Envio e Entregas</p>
                <p>Formas de Pagamento</p>

            </div>

            <div class="rodape-conteudo">

                <h2>CENTRAL DE RELACIONAMENTO</h2>
                <p>Atendimento</p>
                <a>Segunda a Sexta das 8 as 18h</a>
                <p>Telefone</p>
                <p>(62) 3249-5309</p>
                <p>E-mail</p>
                <a>contato@brindes.com.br</a>

            </div>

        </div> <!-- Rodape -->
    
    </div> <!-- Rodape Total -->
	
	<script><?php echo file_get_contents('pw-js/jquery.js');?></script>
    <script><?php echo file_get_contents('pw-js/swiper.min.js');?></script>
    <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
    <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>
	<script>
	 
		(function($) {	
			'use strict';
			window.sr = ScrollReveal();
			sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
		})();
		  
		  
	</script>
</body>

</html>