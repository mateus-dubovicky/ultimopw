<?php
	$dadosPagina["titulo"]   = "Sala de reunião";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Salas de reunião, Sala de Reunião Moderna, Wi-Fi, Salas para reunião,
    Sala Reunião Escritório, Sala reunião Escritório pequena.
    \" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Sala de reunião – All Flags\" />";
    $dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">

	<div class="titulo">
		<h1><i class="far fa-building"></i> <br> Serviços</h1>
	</div>

	<div class="box-conteudo">

		<div class="box-txt">
			<ul>
                <li><i class="fas fa-check"></i> Salas de Reuniões</li>
                <li><i class="fas fa-check"></i> Internet Wi-Fi </li>
                <li><i class="fas fa-check"></i> Domiciliação Fiscal e Comercial</li>
                <li><i class="fas fa-check"></i> Receção de Correspondência</li>
                <li><i class="fas fa-check"></i> Estaçao de Trabalho rotativa </li>
                <li><i class="fas fa-check"></i> Estaçao  de Trabalho fixa individual </li>
                <li><i class="fas fa-check"></i> Número de telefone individual</li>
                <li><i class="fas fa-check"></i> Sala de reuniões</li>
                <li><i class="fas fa-check"></i> Espaço para treinamentos e eventos</li>
            </ul>
		</div>

	</div>
	
</div>