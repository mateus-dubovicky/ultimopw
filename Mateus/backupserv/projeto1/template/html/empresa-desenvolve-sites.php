
<?php
	$dadosPagina["titulo"]   = "Empresa que desenvolve de sites -  ProjetoWeb"; 
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Desenvolvimento de páginas profissionais, empresa de criação de site, empresa de sites, empresa de sites, sobre empresa sites.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Empresa\" />";
?>

<div class="conteudo">
    <div class="conteudo-sobre">    


        <div class="box-interno">

        <h1>Empresa Projeto Web Site - Criação de Sites</h1>
        <p>Criação de Site para divulgar sua empresa, produtos e serviços 24 horas por dia.</p>
        <br>

        <h1>Criação de Sites Profissionais com Atendimento para Todo o Brasil.</h1>

        <p>A Empresa Projeto Web Site, está Localizada em Vinhedo, Região de Campinas - São Paulo - SP. Temos Muita Experiência em Atendimento à Distância, atualmente temos mais de 500 clientes fidelizados em todo o Brasil, sua maioria no Estado de São Paulo e já fizemos site para quase Todos os Estados do Brasil.</p>

        <p>Baseada na metodologia Atender Bem Para Atender Sempre, a Projeto Web se preocupa com a satisfação do cliente em primeiro lugar. Uma empresa inovadora, que preza pelo bom atendimento e preço justo, visamos atender todas as necessidades de sua empresa para obter os melhores resultados. </p>

        <p>
           Criação de sites pessoais, institucionais, profissionais e blogs. Oferecemos os serviços de Hospedagem e Manutenção de sites, Otimização de Sites (SEO), Sites Responsivos, Criação de Logotipo, Cartão de Visita e Arte para Newsletter.
           Com o diferencial de criar uma prova sem compromisso, deixamos a empresa conectada com o processo de desenvolvimento do seu projeto, ou seja, a Projeto Web cria uma prova de como vai ficar o seu novo site. Assim, só fechamos uma parceria caso você realmente aprove o layout criado.
        </p>

        </div>

        <div class="box-interno-dois">

        <button class="buttonSolicitar">Solicite seu orçamento agora mesmo</button>

        <img src="[template]/pw-images/criacao-de-sites-para-empresas-em-campinas-sao-paulo-sp.png" alt="Numero um em qualidade">

        </div>

        <div class="box-interno-tres">

        <p>
        Pensando nos melhores resultados para seu site e sua empresa, temos como objetivo criar sites de impacto, que contenham um marketing digital integrado. É com base neste pensamento que nossos clientes, sites e empresas estão bem posicionados nos buscadores e contentes com um site que ajuda no crescimento do seu negócio.
        Estamos constantemente aprimorando as soluções que oferecemos, sempre olhando para o principal: as novas necessidades das empresas, clientes e mercado.
        Esperamos aprimorar cada vez mais nossos produtos e serviços e assim gerar resultados usando a internet como plataforma, seja para vender, divulgar ou administrar grandes volumes de informação de sua empresa, produto ou serviço.
        Estamos à disposição para atendê-lo e fechar mais uma parceria.
        </p>

        <div class="imgDivTres">
            <img src="[template]/pw-images/agencia-web-clientes-ativos-em-todo-brasil.png" alt="Brasil">
        </div>

        </div>

        <div class="box-interno-quatro">

            <h1>Quer Fazer um Site para Sua Empresa?</h1>
            <p>Confira as principais informações para fazer um site para sua empresa. Criação de Site Profissional para sua Empresa.</p>

            <h2>Primeiro passo</h2>
            <p>O Primeiro passo é encontrar uma empresa de web designer que lhe dê um atendimento de qualidade. O atendimento da empresa é um dos pontos mais importantes, pois são as 2 empresas em conjunto que vão criar o site.</p>
            <p>Portanto pesquise sobre a empresa, veja seu portfólio de sites, se possível entre em contato com alguns clientes e solicite mais informações sobre a empresa que deseja contratar.</p>    
            <p>Você tem que se sentir confortável e à vontade para fazer todos os questionamentos, obter informações é a forma mais fácil de entender melhor sobre a criação de site e sentir confiança na empresa contratada. Desta forma a chance da criação do site dar certo aumenta muito.</p>

            <h2>Site, Credibilidade e Visibilidade para sua Empresa</h2>
            <p>Com o avanço da tecnologia e a facilidade de acesso de celulares à internet, hoje a maioria das pessoas quando deseja comprar algum produto ou contratar um serviço, pesquisa na internet. Portanto ter um site é indispensável.</p>
            <p>O site deve ser bem feito, ter uma fácil navegação, ser rápido e otimizado para o Google (atualmente detentor de mais de 95% das buscas na web). Criar um site profissional vai dar a chance de sua empresa mostrar seu potencial para pessoas que pesquisem por seu produto ou serviço na internet. A criação de um site profissional consiste em diversas técnicas de codificação (programação), arte e design para cada página de seu site e otimização. As 3 fases são muito importantes devido a interação que seu site vai ter com pessoas e os robôs dos mecanismos de pesquisas (Google). Uma fase mal feita e seu site pode não obter resultados.</p>
            <p>Portanto procure um profissional de web design para lhe ajudar na criação do site, isso com certeza fará diferença.</p>

        </div>

        <div class="box-interno-cinco">
            
        <p class="tituloMaior"> ARTE E DESIGN  <img src="[template]/pw-images/sites-para-empresas-em-campinas-sao-paulo-sp.jpg" alt="Logo ProjetoWeb"> </p>
           
        <p class="tituloMaior"> PROGRAMAÇÃO <img src="[template]/pw-images/sites-para-empresas-em-campinas-sao-paulo-sp.jpg" alt="Logo ProjetoWeb"> </p>

        <p class="tituloMaior"> OTIMIZAÇÃO </p>

        </div>

        <div class="box-interno-seis">
            
        <h2>Conteúdo do Site e Marketing</h2>
        <p>É essencial que seu site tenha um conteúdo de qualidade, você deve fazer textos completos, que explique bem cada produto e serviço oferecido e sempre utilize imagens que façam referência ao conteúdo. Isso atrai a atenção do leitor e facilita a compreensão.</p>
        <p>Com um conteúdo bem montado, a segunda parte é o marketing. É neste passo que muitas empresas acabam pecando, por falta de informação ou experiências ruins que tiveram. O Marketing para seu site é indispensável, sem ele seu site ficará parado na internet e com muita dificuldade alguém irá encontra-lo. É necessário fazer ações de divulgação, campanhas nas redes sociais e links patrocinados em buscadores. Divulgue seu site em todas as mídias impressas da sua empresa e utilize o e-mail do seu site. Tudo isso vai fazer seu site se movimenta e com o tempo ele pode começar a aparecer nos resultados de pesquisas do Google.</p>
        <p>Um dos pontos mais relevantes para o Google é a quantidade de acessos que o site tem, se seu site tiver muitos acessos, ele provavelmente aparecerá em evidência. O site é uma ferramenta que você utilizará para gerar contados, leads, vendas.</p>
        

        <h2>Domínio e E-mails para seu Site</h2>
        <p>Esta decisão deve ser feita com cautela, visto que será o nome de sua empresa na internet. Escolha um nome fácil de se lembrar e simples de digitar. Por exemplo: nosso site é <a href="www.projetowebsite.com.br">www.projetowebsite.com.br</a>  ou <a href="www.projetoweb.com.br"> www.projetoweb.com.br</a>, os e-mails são: <a href="mailto:comercial@pojetoweb.com.br">comercial@pojetoweb.com.br</a> ou <a href="mailto:comercial@projetowebsite.com.br">comercial@projetowebsite.com.br</a>. Fácil de lembrar, simples de se ler e escrever.</p>
        <p>Se possível coloque uma palavra chave em seu domínio, uma palavra que remeta ao seu produto e serviço, isso ajudará nas buscas do seu site no futuro.0</p>
        
        </div>

        <div class="img-box-interno-seis">
            <img src="[template]/pw-images/hospedagem-para-sites.png" alt="Servidores de hospedagem">
        </div>

        <div class="box-interno-sete">
            
        <h2>Hospedagem para seu domínio</h2>
        <p>A hospedagem de seu domínio é o coração do seu site e e-mails, se ela for lenta, prejudicará o site e se parar, tudo sairá do ar. Portanto sempre contrate uma hospedagem profissional, 
           confiável e de qualidade. Teste o servidor e verifique se ele é rápido, isso influirá na navegação de seu site. 
           O atendimento é de extrema importância neste caso, já que você somente precisará deles quando der algum problema. 
           Então tente obter mais informações sobre a empresa de hospedagem e converse com alguns clientes para verificar a qualidade do serviço e rapidez de atendimento.</p>
        

        <h2>Fazer um Site com Profissional ou Fazer você mesmo um site Grátis?</h2>
        <p>Existe diferença? Claro que é diferente fazer um site com um profissional de web design e uma pessoa leiga desenvolver um site em uma ferramenta gratuita.</p>
        <p>Você deve analisar para que você deseja o site, se for apenas para ter um cartão visita na internet, ou um site pessoal, o site grátis deve atender. Agora se você deseja obter resultados com o site, crescer como empresa, divulgar na internet, você precisa da criação de um site profissional.</p>
        <p>O site profissional, criado por um web designer qualificado, terá uma qualidade muito superior à do grátis, devido ao trabalho do layout, que pode ser personalizado e as imagens devidamente trabalhadas, ajustadas e otimizadas. Ter uma programação toda validada e que siga as técnicas e regras exigidas pelos buscadores é de extrema importância, isso você só conseguirá na criação de um site profissional.</p>
        <p>Já testou um site Grátis nas principais ferramentas da internet? Lhe convido a fazer isso, para você verificar quantos erros de codificação, otimização e outros mais acontecem, são muitos.</p>
        <p>Teste um site que você sabe que é grátis no:</p>

        <p>W3C – Ferramenta de Validação de codificação: <a href="https://validator.w3.org/">https://validator.w3.org/</a></p>
        <p>Insights – Ferramenta do Google que valida vários pontos do site, de imagens a codificação e hospedagem: <a href="https://developers.google.com/speed/pagespeed/insights/?hl=pt-BR">https://developers.google.com/speed/pagespeed/insights/?hl=pt-BR</a></p>
        <p>Web Page Test – Ferramenta muito utilizada que faz uma avaliação Geral de seu site: <a href="https://www.webpagetest.org/ ">https://www.webpagetest.org/ </a> (* no Test Location, coloque São Paulo).</p>
        </div>

        <div class="box-interno-buttonAmostra">

        <button class="buttonAmostra">
        <i class="far fa-envelope"></i>
            Entre em contato com a Projeto Web para solicitar uma amostra do seu site.
        </button>    

        </div>

    </div>
</div>