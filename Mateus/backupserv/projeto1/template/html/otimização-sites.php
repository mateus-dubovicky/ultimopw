<?php
	$dadosPagina["titulo"]   = "Otimização de sites -  ProjetoWeb"; 
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\" Serviços de sites, otimização de sites, hospedagem de sites, criação de logos, manutenção de sites, site administrável, site responsivo.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Criando sites\" />";
?>


<div class="conteudo">
    <div class="conteudo-sobre">

        <div class="box-interno">

            <h1>Agência Digital, Desenvolvimento Web, Marketing</h1> 
            <p>Quando se trata de Sites Institucionais, com a finalidade de mostrar seus serviços e/ou produtos, os serviços de Criação, Hospedagem, Manutenção (mensal e avulsa) e Otimização para Google são nosso ponto forte.</p>
            <p>Além de serviços diretamente relacionados à internet, desenvolvemos também, Logotipo, Cartão Visita e Arte para E-mail Marketing.</p>
            <p>Estaremos sempre à disposição para atendê-lo da melhor maneira possível.</p>
            <p>Se desejar saber mais sobre cada serviço oferecido pela Projeto Web, acesse as opções abaixo:</p>
        
        </div>


        <div class="box-interno-dois">

            <div class="servico">
                <div class="circulo">
                    <i class="fas fa-desktop"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Criacao de sites</a></div>
                
            </div>

            <div class="servico">
                <div class="circulo">
                    <i class="fas fa-pencil-alt"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Criação de Logo, Logotipo, Logomarca</a></div>
                
            </div>
            <div class="servico">
                <div class="circulo">
                    <i class="fas fa-search"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Otimização Google - SEO</a></div>
                
            </div>

        </div>


        <div class="box-interno-dois">

        <div class="servico">
                <div class="circulo">
                    <i class="fas fa-mobile-alt"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Site com Design Responsivo</a></div>
                
            </div>
            
            <div class="servico">
                <div class="circulo">
                    <i class="fas fa-server"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Hospedagem de Sites</a></div>
                
            </div>

            <div class="servico">
                <div class="circulo">
                    <i class="fas fa-cogs"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Manutenção Mensal de Sites</a></div>
                
            </div>
        </div>


        <div class="box-interno-dois">

        <div class="servico">
                <div class="circulo">
                    <i class="fab fa-wordpress"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Site Administrável</a></div>
                
            </div>
            
            <div class="servico">
                <div class="circulo">
                    <i class="far fa-id-card"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Criação de Cartão Visita</a></div>
                
            </div>

            <div class="servico">
                <div class="circulo">
                    <i class="fas fa-envelope"></i>
                </div>
                
                <div class="tituloServicos"><a href="">Criação de Arte para E-mail Marketing</a></div>
                
            </div>
        </div>


    </div>
</div>