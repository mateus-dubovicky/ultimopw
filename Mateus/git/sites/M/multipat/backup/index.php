<?php get_header(); ?>

<!-- Start WOWSlider.com BODY section -->
<!-- add to the <body> of your page -->
<div id="wowslider-container1">
	<div class="ws_images">
		<?php $fields = get_fields(10); ?>

		<ul>
			<?php foreach ($fields as $field) { ?>
				<?php if ($field['url'] != "") { ?>
					<li><img src="<?php echo $field['url']; ?>" alt="<?php echo $field['alt']; ?>" title="<?php echo $field['alt']; ?>" /></li>
				<?php } ?>
			<?php } //FECHA FOREACH $fields
			?>
		</ul>
	</div>

	<div class="ws_shadow"></div>
</div>
<!-- End WOWSlider.com BODY section -->

<div class="box-01-total">

	<a class="entrar" href="<?php echo get_permalink(93); ?>" title="" data-icon="empresa">
		<div class="box-01">

			<div class="conteudo">
				<div class="titulo">BEM-VINDO AO NOSSO SITE</div>
				<div class="linha">
					<div class="barra"></div>
				</div>
				<div class="texto">
					Com sede em Campinas/SP, o Laboratório Multipat atua há mais de 20 anos no segmento de anatomia patológica, citologia diagnóstica e patologia molecular. Fundado em 1997 a partir da união de conhecimentos e do espírito empreendedor de dois docentes da Unicamp, Profa. Dra. Miriam Trevisan, especialista em patologia do trato gastro-intestinal e citopatologia, e Prof. Dr. José Vassallo, especialista em hematopatologia e um dos pioneiros na implementação de imunoistoquímica no país.
				</div>
				<div class="entrar">Saiba mais</div>
			</div>
			<div class="img">
				<img src="<?php bloginfo('template_directory'); ?>/pw-images/box-01.jpg" alt="" title="" />
				<div class="info"><i class="far fa-thumbs-up"></i><br />"Atender Bem Para Atender Sempre"</div>
			</div>

		</div>
	</a>

</div>

<div class="box-02-total">

	<div class="box-02">

		<a class="conteudo" href="" title="" data-icon="empresa">
			<div class="item">

				<div class="img"><img src="<?php bloginfo('template_directory'); ?>/pw-images/convenio.png" alt="" title="" /></div>
				<div class="titulo">Convênios</div>
			</div>
		</a>
		<a class="conteudo" href="<?php echo get_permalink(96); ?>" title="" data-icon="empresa">
			<div class="item">

				<div class="img"><img src="<?php bloginfo('template_directory'); ?>/pw-images/exames.png" alt="" title="" /></div>
				<div class="titulo">Exames e Serviços  </div>
			</div>
		</a>
		<a class="conteudo" href="" title="" data-icon="empresa">
			<div class="item">

				<div class="img"><img src="<?php bloginfo('template_directory'); ?>/pw-images/reaquisicao.png" alt="" title="" /></div>
				<div class="titulo">Requisições para Exames</div>
			</div>
		</a>

	</div>

</div>

<div class="box-03-total">

	<div class="box-03">

		<div class="linha">
			<div class="barra"></div>
		</div>
		<div class="titulo">ENTRE EM CONTATO</div>

		<form action="mail-contato.php" method="post" class="formhome">
			<div class="item">
				<input name="campo[Nome]" type="text" placeholder="Nome:" />
				<input name="campo[E-mail]" type="text" placeholder="E-mail:" />
				<input name="campo[Telefone]" type="text" placeholder="Telefone:" />
			</div>
			<div class="item">
				<textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
			</div>
			<input class="submit" type="submit" value="Enviar" />
		</form>

	</div>

</div>

<div class="mapa">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.041002375837!2d-47.054869349213526!3d-22.83797248497345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8c425ce89e383%3A0x530a3f2b065b2af1!2sAv.%20Profa.%20Ana%20Maria%20Silvestre%20Adade%2C%20407%20-%20Parque%20das%20Universidades%2C%20Campinas%20-%20SP%2C%2013086-130!5e0!3m2!1spt-BR!2sbr!4v1620678354781!5m2!1spt-BR!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<?php get_footer(); ?>