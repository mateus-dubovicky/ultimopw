<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <!-- <style><?php echo file_get_contents('template/pw-css/style.css');?></style> -->
    <link rel="stylesheet" type="text/css" href="[template]/pw-css/style.css" />
    <link rel="stylesheet" type="text/css" href="pw-css/swiper.min.css" />
    <link rel="stylesheet" href="pw-font-awesome/css/all.css">  
    
    [css]
    
    <style>
        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }
    </style>
</head>

<body>

	<div class="topo-total">

        <div class="topo-contato">
        <p> <i class="fas fa-envelope"></i> comercial@projetoweb.com.br</p>

        <div class="topo-redes">
            <i class="fab fa-facebook"></i>
            <i class="fab fa-instagram"></i>
        </div>
        </div>

    
    </div> <!-- Topo Total -->
    
    <div class="menu-total">

        <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo.png" alt="" title="" /></a>
        </div> <!-- Logo -->
	
		<div class="menu-resp"></div> <!-- Menu Resp -->
    
    	<div class="menu">
        	<ul>
            	<li><a href="[url]/" title="">Home</a></li>
                <li><a href="[url]/" title="">Empresa</a></li>
                <li><a href="[url]/" title="">Serviços</a></li>
                <li><a href="[url]/" title="">Portfólio</a></li>
                <li><a href="[url]/" title="">Contato</a></li>
            </ul>
        
        </div> <!-- Menu -->
    
    </div> <!-- menu Total -->

<div class="global">

	[conteudo]
	
</div> <!-- global -->



        <div class="rodape">
            <div class="rodape-contato">
                <p> <i class="fas fa-phone"></i>    (19) 3826-2294 / 98220-7552</p>
                <p> <i class="fas fa-envelope"></i> comercial@projetoweb.com.br</p>
            </div>

            <div class="rodape-redes">
                <i class="fab fa-facebook"></i>
                <i class="fab fa-instagram"></i>
            </div>
                
        </div>


	
	<script><?php echo file_get_contents('pw-js/jquery.js');?></script>
    <script><?php echo file_get_contents('pw-js/swiper.min.js');?></script>
    <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
    <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>
	<script>
	 
		(function($) {	
			'use strict';
			window.sr = ScrollReveal();
			sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
		})();
		  
		  
	</script>
    <script>
        var swiper = new Swiper('.swiper-container', {
        pagination: '',
        slidesPerView: 1,
        loop: true,
        autoplay: 3000,
        speed: 2000,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 0,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeMode: false,
        breakpoints: {
            // when window width is >= 500px
            500: {
                slidesPerView: 1,
                slidesPerColumn: 1,
            }
        }
    });
    </script>
</body>

</html>