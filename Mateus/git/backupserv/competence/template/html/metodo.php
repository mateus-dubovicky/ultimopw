<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>

<div class="conteudo-pages">
    <h1>MÉTODO PEDAGÓGICO</h1>
</div>

<div class="page">
    <div class="row single">
        <div class="box-txt">
            <p>Adotamos o método construtivista que valoriza o ser humano, favorece a reconstrução do conhecimento de forma contextualizada em que alunos e professores são pesquisadores de informações e dados que vão permitir a compreensão para construir um mundo melhor, baseada nos cinco pilares de sustentação do aprender, que são: </p>
            <ol>
                <li>Aprender a conhecer</li>
                <li>Aprender a fazer</li>
                <li>Aprender a conviver</li>
                <li>Aprender a ser</li>
                <li>Aprender a aprender</li>
            </ol>
            <p>A metodologia é direcionada para um sistema centrado no aluno, utilizando técnicas de dinâmica de grupo para avaliação do ser humano. </p>
        </div>
    </div>
</div>