<?php
	$dadosPagina["titulo"] = "Página Interna";
?>
<div class="conteudo-pages">

        <div class="texto-pages">
        	<h1>Contato</h1>
        
             <div class="contato">
        
                <div class="required">
                    <img src="<?php bloginfo('template_directory');?>/images/required.png" alt="" title="" /> Campos obrigatórios.
                </div> <!-- Required -->
                
                <form action="mail-contato.php" method="post">
                
                    <select name="campo[Área]">
                        <option value="">-- Selecione a Área --</option>
                        <option value="Geral">Geral</option>
                        <option value="Dúvidas e Reclamações">Dúvidas e Reclamações</option>
                        <option value="Produtos">Produtos</option>
                    </select>
                    <input name="campo[Nome]" placeholder="Nome:" type="text" required="required" />
                    <input name="campo[Telefone]" placeholder="Telefone:" type="text" required="required"/>
                    <input name="campo[E-mail]" placeholder="E-mail:" type="text" required="required"/>
                    <input name="campo[Cidade]" placeholder="Cidade:" type="text" />
                    <input name="campo[Estado]" placeholder="Estado:" type="text" />
                    <textarea name="campo[Mensagem]" placeholder="Mensagem:" required="required"></textarea>
                    <input class="submit" value="Enviar" type="submit"/>
                
                </form>
            
            </div> <!-- Contato -->
            
                
        </div><!-- Texto Pages -->
</div> <!-- conteudo pages -->