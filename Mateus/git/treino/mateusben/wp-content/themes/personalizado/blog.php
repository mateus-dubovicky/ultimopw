<?php /* Template name: Blog */ ?>
<?php get_header(); ?>

<div class="titulo-total pages"></div>

<div class="page-blog">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div class="titulo">
				<?php the_post_thumbnail(); ?>
				<h1><?php echo get_the_title(); ?></h1>
			</div>

        <?php endwhile; ?>
    <?php endif; ?>

    


<div class="blog-total">

    <div class="blog">


            <?php

                // the query to set the posts per page to 3
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array('posts_per_page' => 5, 'paged' => $paged, 'post_type' => 'post');
                query_posts($args); ?>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>



        <a href="<?php echo get_permalink(); ?>" title="<?php the_title_attribute(); ?>">

            <div class="post">
        
            <h2> ><?php echo get_the_title(); ?> </h2>

            <?php echo get_the_post_thumbnail(); ?>

            <p>  <?php echo get_the_excerpt(); ?>  </p>

        </div>

        </a>


        <?php endwhile; ?>
                    <!-- pagination -->

                    <div class="proxima-pagina"><?php next_posts_link(); ?></div>
                    <div class="pagina-anterior"><?php previous_posts_link(); ?></div>

                    <?php else : ?>
                    <!-- No posts found -->

        <?php endif; ?>
        
    </div>


    <div class="categorias">
            <h2>Categorias</h2>
                <ul>
                    
                    <?php
                        $categories = get_categories(array(
                            'post_type' => 'post',
                            'orderby' => 'name',
                            'parent'  => 0
                        ));

                        foreach ($categories as $category) {
                            printf(
                                '<a href="%1$s"><li><i class="fas fa-angle-right"></i>%2$s</li></a>',
                                esc_url(get_category_link($category->term_id)),
                                esc_html($category->name)
                            );
                        } ?>

                </ul>
    </div>

</div>

        

</div> <!-- conteudo-pages -->

<?php get_footer(); ?>