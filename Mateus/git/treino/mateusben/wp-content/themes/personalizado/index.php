<?php get_header(); ?>

<div class="global">

	<div class="box-01">

		<div class="banner-principal">

			<img src="<?php bloginfo('template_directory'); ?>/pw-images/trocaDeMensagens.jpg" alt="Mensagens de Celular" title="Mensagens de celular">

		</div>

	</div>

	<div class="box-02">

		<div class="servico-qualidade">

			<div class="qualidade">
				<i class="fas fa-stopwatch"></i>
				<h2>Agilidade e rápidez</h2>
			</div>

			<div class="qualidade">
			<i class="fas fa-certificate"></i>
				<h2>Preço Competitivo</h2>
			</div>

			<div class="qualidade">
				<i class="fas fa-truck"></i>
				<h2>Frete Grátis</h2>
			</div>

			<div class="qualidade">
				<i class="fas fa-credit-card"></i>
				<h2>Pagamento Parcelado</h2>
			</div>

			<div class="qualidade">
				<i class="fas fa-comment-dots"></i>
				<h2>Resposta Rápida</h2>
			</div>

		</div>

	</div>


	<div class="box-03">

			<div class="tiulo-box-02">
				<h2> ESCOLHA A CATEOGORIA DESEJADA </h2>
			</div>

			<div class="box-imagens">
				<div class="imagem-produto">
					<img src="<?php bloginfo('template_directory'); ?>/pw-images/primeira-piramide.jpg" alt="Piramide Egito" title="Piramide Egito">
					<p>Seneferu</p>
				</div>

				<div class="imagem-produto">
					<img src="<?php bloginfo('template_directory'); ?>/pw-images/segunda-piramide.jpg" alt="Piramide Egito" title="Piramide Egito">
					<p>Quéops</p>
				</div>

				<div class="imagem-produto">
					<img src="<?php bloginfo('template_directory'); ?>/pw-images/terceira-piramide.jpg" alt="Piramide Egito" title="Piramide Egito">
					<p>Tanquerés</p>
				</div>
			</div>


			<div class="tiulo-box-02">
				<h2> ALGUMAS NOVIDADES </h2>
			</div>

			<div class="box-imagens novidades">


							<?php $posts = get_posts(array('post_type' => 'produtos', 'numberposts' => -1 ));?>
							<?php foreach($posts as $post){ ?>
							<?php setup_postdata($post); ?>


							<div class="box-imagem-produto">
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" title="" />
								<h2 class="subtitulo"> <?php echo get_the_title(); ?>  </h2>
								<p> <?php echo get_the_excerpt(); ?> </p>
							</div>

							
							<?php } ?>

			</div>


			<div class="tiulo-box-02">
				<h2> ALGUNS DOS NOSSOS CLIENTES </h2>
			</div>

			<div class="box-imagens">
					<div class="imagem-produto">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoNissan.png" alt="Nissan" title="Logo Nissan">
					</div>

					<div class="imagem-produto">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoBmw.png" alt="BMW" title="Logo BMW">
					</div>

					<div class="imagem-produto">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoAlfaRomeo.png" alt="Alfa Romeo" title="Logo Alfa Romeo">
					</div>

					<div class="imagem-produto">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoDodge.png" alt="Dodge" title="Logo Dodge">
					</div>

					<div class="imagem-produto">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoAudi.png" alt="Audi" title="Logo Audi">
					</div>

			</div>

	</div>

	<div class="box-final">
		<div class="box-final-conteudo">

			<div class="final-texto">
				<h2>Conheça nossa loja virtual </h2>
				<p>Produtos sem personalização, a pronta entrega e sem quantidade mínima</p>
			</div>

			<div class="box-final-botao">
			<button>Clique para acessar</button>
			</div>
			
		</div>
		
	</div>
    
    
</div> <!-- global -->



<?php get_footer(); ?>


		