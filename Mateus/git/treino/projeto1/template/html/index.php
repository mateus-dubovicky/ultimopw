<?php
	$dadosPagina["titulo"]   = "Criação de sites - ProjetoWeb"; 
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Empresa de criação site, otimização de sites, lojas virtuais, criação de websites, criar sites, contratar empresa site, sites para empresa.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Criação de Sites\" />";
    $dadosPagina["metas"][2] = "";
    $dadosPagina["css"] = "<style></style>";
?>
    <div class="banner-total">
        <div class="banner">
            <!-- Start WOWSlider.com BODY section -->
            <!-- add to the <body> of your page -->
            <div id="wowslider-container1">
                <div class="ws_images">
                    <ul>
                        <li><img src="[template]/pw-slider-data/images/1.jpg" alt="1" title="1" id="wows1_0" /></li>
                        <li><img src="[template]/pw-slider-data/images/2.jpg" alt="2" title="2" id="wows1_1" /></li>
                    </ul>
                </div>

                <div class="ws_shadow"></div>
                
            </div>
            <!-- End WOWSlider.com BODY section -->
            <div class="texto">SEJA BEM-VINDO</div>

        </div>
        <!-- banner -->
    </div>
    <!-- banner total -->

    <div class="conteudo-pages">

        <div class="box-01-total">

            <div class="box-01">

                <div class="linha-01">


                    <div class="conteudo">
                        <a href="">
                            <div class="item">

                                <div class="titulo">
                                    BEM-VINDO
                                </div>
                                <!-- titulo -->

                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id venenatis metus. Quisque eleifend tortor et dolor vehicula, eget lacinia sapien facilisis.
                                </div>
                                <!-- texto -->

                            </div>
                            <!-- item -->
                        </a>
                        <!-- a item-->
                    </div>
                    <!-- conteudo -->


                    <div class="img">

                        <div class="item">

                            <img src="[template]/pw-images/index-01.jpg" alt="" />

                        </div>
                        <!-- item -->

                    </div>
                    <!-- img -->


                </div>
                <!-- linha 01 -->


                <div class="linha-02">


                    <div class="conteudo esquerda">
                        <a href="">
                            <div class="item">

                                <div class="img"><i class="fas fa-laptop"></i></div>
                                <!-- img -->

                                <div class="titulo">
                                    CRIAÇÃO
                                </div>
                                <!-- titulo -->

                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id venenatis metus. Quisque eleifend tortor et dolor vehicula, eget lacinia sapien facilisis.
                                </div>
                                <!-- texto -->

                            </div>
                            <!-- item -->
                        </a>
                        <!-- a item -->
                    </div>
                    <!-- conteudo -->


                    <div class="conteudo centro">
                        <a href="">
                            <div class="item">

                                <div class="img"><i class="fas fa-server"></i></div>
                                <!-- img -->

                                <div class="titulo">
                                    HOSPEDAGEM
                                </div>
                                <!-- titulo -->

                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id venenatis metus. Quisque eleifend tortor et dolor vehicula, eget lacinia sapien facilisis.
                                </div>
                                <!-- texto -->

                            </div>
                            <!-- item -->
                        </a>
                        <!-- a item -->
                    </div>
                    <!-- conteudo -->


                    <div class="conteudo direita">
                        <a href="">
                            <div class="item">

                                <div class="img"><i class="fas fa-cogs"></i></div>
                                <!-- img -->

                                <div class="titulo">
                                    OTIMIZAÇÃO
                                </div>
                                <!-- titulo -->

                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id venenatis metus. Quisque eleifend tortor et dolor vehicula, eget lacinia sapien facilisis.
                                </div>
                                <!-- texto -->

                            </div>
                            <!-- item -->
                        </a>
                        <!-- a item -->
                    </div>
                    <!-- conteudo -->


                </div>
                <!-- linha 01 -->


                <div class="linha-03">


                    <div class="img">

                        <div class="item">

                            <img src="[template]/pw-images/index-02.jpg" alt="" />

                        </div>
                        <!-- item -->

                    </div>
                    <!-- img -->


                    <div class="conteudo">
                        <a href="">
                            <div class="item">

                                <div class="titulo">
                                    COMO FUNCIONA
                                </div>
                                <!-- titulo -->

                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id venenatis metus. Quisque eleifend tortor et dolor vehicula, eget lacinia sapien facilisis.
                                </div>
                                <!-- texto -->

                            </div>
                            <!-- item -->
                        </a>
                        <!-- a item -->
                    </div>
                    <!-- conteudo -->


                </div>
                <!-- linha 03 -->

            </div>
            <!-- box 01 -->


        </div>
        <!-- box 01 total -->


        <div class="box-02-total">

            <div class="box-02">
                <div class="item"><img src="[template]/pw-images/galeria/01.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/02.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/03.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/04.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/05.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/06.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/07.jpg" alt="" /></div>
                <div class="item"><img src="[template]/pw-images/galeria/08.jpg" alt="" /></div>
            </div>
            <!-- box 02 -->

        </div>
        <!-- box 02 total -->


        <div class="box-03-total">

            <div class="box-03">

                <div class="titulo">
                    ENTRE EM CONTATO
                </div>
                <!-- titulo -->

                <div class="formulario">

                    <form action="pw-form.php" method="post">

                        <div class="form-esquerda">

                            <input type="text" name="campo[Nome]" placeholder="Nome">
                            <br>
                            <input type="text" name="campo[Telefone]" placeholder="Telefone">
                            <br>
                            <input type="text" name="campo[E-mail]" placeholder="E-mail">
                            <br>

                        </div>
                        <!-- form esquerda -->

                        <div class="form-direita">
                            <input type="text" class="mensagem" name="campo[Mensagem]" placeholder="Mensagem">
                        </div>
                        <!-- form direita -->

                        <div class="botao">
                            <input class="submit" type="submit" value="Enviar">
                        </div>
                        <!-- botao -->

                    </form>

                </div>
                <!-- formulario -->

            </div>
            <!-- box 03 -->

        </div>
        <!-- box 03 total -->

        <div class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.924944207389!2d-46.985931884536924!3d-23.026527884951076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf32a893c64db7%3A0x52a38f729cdadb1a!2sAv.+Dourado%2C+233+-+Vila+Sao+Sebastiao%2C+Vinhedo+-+SP%2C+13280-000!5e0!3m2!1spt-BR!2sbr!4v1494860599346" height="300" style="border:0; margin-bottom: -3px;" allowfullscreen></iframe>

        </div>
        <!-- mapa -->



<div class="mapa-resp">
<a target="_blank" href="https://www.google.com/maps?ll=-23.02658,-46.983817&z=16&t=m&hl=pt-BR&gl=BR&mapclient=embed&q=Av.+Dourado,+233+-+Res.+Aqu%C3%A1rio+Vinhedo+-+SP+13280-000"><img src="[template]/pw-images/mapa.jpg" alt=""></a>
</div>
<!-- mapa -->

    </div>
    <!-- conteudo index -->
