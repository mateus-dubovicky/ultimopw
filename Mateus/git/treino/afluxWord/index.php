<?php get_header(); ?>



<div class="conteudo-pages">	
	
		<div class="global">

			<div class="box-banner">
				<div class="texto-banner">
					<h2>Soluções em <br> <span>tratamento de água</span> </h2>
				</div>
			</div>


			<div class="box-01-total">

				<div class="box-01">  

					<div class="box-01-conteudo">
					<h2>SEJA BEM VINDO A AFLUX BRASIL</h2>

					</div>


					<div class="box-01-conteudo">
					<h3>UMA EMPRESA PARA ATENDER SUAS DEMANDAS EM PRODUTOS E SISTEMAS PARA <br> TRATAMENTO DE ÁGUAS E EFLUENTES, MONTAGENS INDUSTRIAIS E CONSTRUÇÃO CIVIL</h3>

					</div>


					<div class="box-01-conteudo">
					<p>A AFLUX BRASIL, mantém em sua equipe profissionais altamente capacitados para atender as demandas de empresas privadas ou públicas, 
						   em Sistemas de Tratamentos de Águas e Efluentes, Montagens Industriais e Construção Civil, nos diversos segmentos industriais.
						   Em nossos projetos priorizamos o uso de tecnologias avançadas e eficazes, para atendermos os níveis de exigências de nossos clientes.</p>

					</div>

				</div>
			
			</div>
			

			<div class="box-02-total">

				<div class="box-02">

					<div class="box-02-titulo">
						<h2>PRINCIPAIS PRODUTOS</h2>
					</div>

					<div class="box-02-linha">
						<hr>
					</div>

					<div class="box-02-subtitulo">
						<p>Os Insumos e Produtos abaixo a AFlux Brasil mantêm Grandes Estoques, para Entregas Imediatas.</p>
					</div>

					<div class="box-02-conteudo">

						<div class="box-02-imagens">


							<?php $posts = get_posts(array('post_type' => 'produto', 'numberposts' => -1 ));?>
							<?php foreach($posts as $post){ ?>
							<?php setup_postdata($post); ?>


							<div class="box-imagem-produto">
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" title="" />
								<h2 class="subtitulo"> <?php echo get_the_title(); ?>  </h2>
								<p> <?php echo get_the_excerpt(); ?> </p>
							</div>

							
							<?php } ?>


						</div>

						

					</div>

				</div>

			</div>


			<div class="box-banner-dois">

				<div class="banner-dois-conteudo">

					<div class="banner-conteudo-titulo">

							<div class="banner-titulo-titulo">
								<h2>Portfólio</h2>
							</div>

							<div class="box-02-linha">
								<hr>
							</div>

							<div class="imagens-box-banner">

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio.jpg" alt="" title="" />
								</div>

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio2.jpg" alt="" title="" />
								</div>

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio3.jpg" alt="" title="" />
								</div>

								<div class="imagem-portfolio">
									<img src="<?php bloginfo('template_directory'); ?>/pw-images/portfolio4.jpg" alt="" title="" />
								</div>
								
							</div>

					</div>
					
				</div>
			</div>


			<div class="box-03-total">
				<div class="box-03">
					<div class="conteudo-03">

						<div class="titulo-03">
							<h2>CONHEÇA UM POUCO MAIS SOBRE A NOSSA EMPRESA</h2>
						</div>

						<div class="texto-03">
							<h3>Na língua chinesa, a palavra crise apresenta o significado, Oportunidade. Foi exatamente através de uma crise, onde o talento, a técnica, o profissionalismo e o “know how” encontraram a oportunidade para realizar além de uma solução, também exercer uma Paixão!</h3>
							<p>Nosso objetivo é que sejam estudadas novas estratégias de uso sustentável da água, frente às necessidades das populações, ecossistemas e uso em atividades produtivas.</p>
							<p>A AFLUX é uma empresa especialista em desenvolver projetos de engenharia para Sistemas de tratamento de água e efluentes, montagens industriais e construção civil. Projeta, fabrica, instala e opera equipamentos com alta performance, que permitem otimizar as características físico químicas e microbiológicas da água.</p>
						</div>
						
					</div>

					<div class="logoBanner">
						<img src="<?php bloginfo('template_directory'); ?>/pw-images/logoBanner.png" alt="" title="" />
					</div>
					
				</div>
			</div>
			

		</div>

</div> <!-- conteudo pages -->






<?php get_footer(); ?>


		