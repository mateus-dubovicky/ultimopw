<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">    

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> [meta]

    <title>[pagina]</title>

    <link href='favicon.png' rel='shortcut icon' type='image/x-icon' />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    [css]

    <style><?php echo file_get_contents('template/pw-css/style.css');?></style>
    <style><?php echo file_get_contents('pw-css/swiper.min.css');?>

        .logo-pw a {
            text-align: center;
            padding-top: 10px;
        }


    </style>

    <style><?php echo file_get_contents('pw-font-awesome/css/all.css');?></style>


    <link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">


<link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
</head>

<body>

    <div class="topo-total">

        <div class="topo">

            <div class="logo">
                <a href="[url]/" title="">
                    <img src="[template]/pw-images/All-Flags-Logo.png" alt="Coworking All Flags" title="Coworking All Flags" />
                </a>
            </div>

            <div class="menu-total">

                <div class="menu">

                    <ul>
                        <li><a href="[url]/" title="Coworking – All Flags">HOME</a></li>
                        <li><a href="[url]/sala-networking-coworking" title="Sala Networking e Coworking">SOBRE NÓS</a></li>
                        <li><a href="[url]/sala-reuniao" title="Sala de reunião Networking e Coworking">SERVIÇOS</a></li>
                        <li><a href="[url]/parceiras-networking" title="Parcerias Network e Coworking">PARCEIROS</a></li>
                        <li><a href="[url]/" title="Coworking">ESPAÇO NEGÓCIO</a></li>
                        <li><a href="[url]/contacto-sala-cowork-network" title="Contacto de Sala Cowork e Network">CONTACTO</a></li>
                    </ul>

                </div>

            </div>

       <div class="whatsapp-topo">
           <p>Atendimento WhatsApp</p>
           <a href=""><i class="fab fa-whatsapp"></i> + 351 91 823 9379</a>
       </div>
        </div>

    </div>

    <div class="global">

        [conteudo]

    </div>

    <div class="rodape-total">

        <div class="rodape">
             <div class="logo"><a href="[url]/" title=""><img src="[template]/pw-images/logo.png" alt="" title="" /></a></div>

            <div class="info">
                <div class="texto"><i class="fas fa-phone"></i>+351 22 095 9690</div>
                <div class="texto"><i class="fas fa-envelope"></i>info@allflagscoworking.com</div>
            </div>

            <div class="whatsapp-topo fim">
           <p>Atendimento WhatsApp</p>
           <a href=""><i class="fab fa-whatsapp"></i> + 351 91 823 9379</a>
       </div>

            <div class="logo-pw">
                <a href="https://www.projetowebsite.com.br" title="" target="_blank"><img src="[template]/pw-images/logo-pw.png" alt="Criação de Site, Construção de Site, Desenvolvimento Web" title="Criação de Site, Construção de Site, Desenvolvimento Web" /></a>
                <div>
                    <p><a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais" target="_blank">Criação de Sites</a></p>
                    <p>
                        <a href="https://www.projetowebsite.com.br/criacao-de-sites-profissionais " target="_blank"><span>Criação de Sites /</span></a>
                        <a href="https://www.projetowebsite.com.br/  " target="_blank"><span>Criação de Sites/</span></a>
                        <a href="https://www.projetoweb.com.br/marketing-e-conteudo-sobre-site/site" target="_blank"><span>Site/</span></a>
                        <a href="https://www.projetoweb.com.br/criar-site " target="_blank"><span>Criar Site/</span></a>
                        <a href="https://www.projetowebsite.com.br/ " target="_blank"><span>Sites/</span></a>
                        <a href="https://www.projetowebsite.com.br/criacao-de-site-para-empresa" target="_blank"><span>Site para empresas/</span></a>
                        <a href="https://www.projetowebsite.com.br/desenvolvimento-web " target="_blank"><span>Desenvolvimento Web</span></a>
                    </p>
                </div>
            </div>

        </div>

        <div class="faixa-rodape" style="display: none;">
            <?php echo file_get_contents('https://www.projetoweb.com.br/faixa/faixa-rodape-clientes.php');?>
        </div>

    </div>

    <script><?php echo file_get_contents('pw-js/jquery.js');?></script>
    <script><?php echo file_get_contents('pw-js/swiper.min.js');?></script>
    <script><?php echo file_get_contents('pw-js/scrollReveal.js');?></script>
    <script><?php echo file_get_contents('pw-js/javascript.js');?></script>
    <script>
        (function($) {
            'use strict';
            window.sr = ScrollReveal();
            sr.reveal('.topo', {
                duration: 2000,
                origin: 'right',
                distance: '1000px',
                viewFactor: 0.6,
                mobile: false
            }, 100);
            sr.reveal('.box-01', {
                duration: 2000,
                origin: 'bottom',
                distance: '100px',
                viewFactor: 0.3,
                mobile: false
            }, 500);
            
            sr.reveal('.box-02 .item', {
                duration: 2000,
                origin: 'left',
                distance: '50px',
                viewFactor: 0.3,
                mobile: false

            }, 500);
            sr.reveal('.box-03 .titulo', {
                duration: 2000,
                origin: 'top',
                distance: '100px',
                viewFactor: 0.3,
                mobile: false

            }, 500);
            sr.reveal('.box-03 .descricao', {
                duration: 2000,
                origin: 'bottom',
                distance: '100px',
                viewFactor: 0.3,
                mobile: false

            }, 500);
            sr.reveal('.box-03 .conteudo', {
                duration: 2000,
                rotate: {
                    x: 90,
                    y: 0,
                    z: 0
                },
                origin: 'bottom',
                distance: '100px',
                viewFactor: 0.3,
                mobile: false

            }, 500);
            sr.reveal('.box-04', {
                duration: 2000,
                origin: 'left',
                distance: '1000px',
                viewFactor: 0.3,
                mobile: false

            }, 500);
            sr.reveal('.mapa', {
                duration: 2000,
                origin: 'top',
                opacity: 1,
                scale: 1,
                distance: '300px',
                viewFactor: 0.3,
                mobile: false

            }, 500);
        })();

        $(document).ready(function() {
            var hash = window.location.hash;
            if (hash == '#pw_site') {
                $('.faixa-rodape').css('display', 'block');
            }
        });

        function isMobile() {
            var userAgent = navigator.userAgent.toLowerCase();
            return (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1);
        }

        if (isMobile()) {
            jQuery(document).ready(function() {
                var page = $(location).attr('href');
                if (page != 'http://oprojetoweb.com.br/provas/modelos-tela-cheia-programado/3/') {
                    jQuery('html, body').animate({
                        scrollTop: $("h1").offset().top
                    }, 500);

                }
            });
        }
          var swiper = new Swiper('.swiper-container', {
        pagination: '',
        slidesPerView: 1,
        loop: true,
        autoplay: 3000,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 0,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
    </script>
</body>

</html>