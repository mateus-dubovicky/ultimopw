<?php
	$dadosPagina["titulo"]   = "Sala Networking e Coworking";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Networking, Coworking, Espaço Coworking, Espaço Compartilhado, Sala Coworking.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Sala Networking e Coworking\" />";
    $dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">

	<div class="titulo">
		<h1><i class="far fa-building"></i> <br> Sobre Nós</h1>
	</div>

	<div class="box-conteudo dup">

		<img src="[template]/pw-images/coworking-sobre-nos.jpg" alt="Coworking sobre nós" title="Coworking sobre nós">

		<div class="box-txt">
			<p>AllFlags CoWorking surge com o intuito maior de ser uma alternativa efetiva para os usuários ampliarem as suas redes de contatos, aumentando as oportunidades de sucesso do seu negócio.</p>
			<p>Nossos parceiros, como chamamos nossos usuários, contam não só com os benefícios já conhecidos nos outros espaços de escritórios compartilhados, mas sim com um ambiente criado para fazer networking, trocar experiências, divulgar o seu negócio e ampliar o desenvolvimento do mesmo.</p>
			<p>Além do ambiente adequado e flexível para atender a qualquer tipo de negócio, AllFlaggs auxilia os parceiros na divulgação dos seus negócios, oferecendo o espaço negócios, a nossa vitrine virtual e física para nosso parceiro fazer a sua divulgação, colocar a "Bandeira" da empresa, dados de contato e informações que ele considere importantes.</p>
			<p>Traga sua "Bandeira" para AllFlags e venha ampliar sua esfera de contatos!</p>
			<p><span>Você tem a sua Bandeira, nós temos todas!</span></p>
		</div>

	</div>
	
</div>
