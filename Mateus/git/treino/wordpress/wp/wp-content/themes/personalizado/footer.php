<?php wp_footer(); ?>

<div class="rodape-total">
    
	<div class="rodape">


		<div class="rodape-logotipo">
	
			<div class="logo"><a href="[url]/" title=""><img src="<?php bloginfo('template_directory'); ?>/pw-images/logoBen.jpg" alt="" title="" /></a></div> <!-- Logo -->

		</div> <!-- Rodape Logoipo -->


		<div class="rodape-conteudo">

			<h2>Institucional</h2>
			<p>Home</p>
			<p>Sobre nós</p>
			<p>Produto</p>
			<p>Contato</p>

		</div>

		<div class="rodape-conteudo">

			<h2>AJUDA E SUPORTE</h2>
			<p>Política e Privacidade</p>
			<p>Dúvidas Frequentes</p>
			<p>Termos de Uso</p>
			<p>Política de Envio e Entregas</p>
			<p>Formas de Pagamento</p>

		</div>

		<div class="rodape-conteudo">

			<h2>CENTRAL DE RELACIONAMENTO</h2>
			<p>Atendimento</p>
			<a>Segunda a Sexta das 8 as 18h</a>
			<p>Telefone</p>
			<p>(62) 3249-5309</p>
			<p>E-mail</p>
			<a>contato@brindes.com.br</a>

		</div>

	</div> <!-- Rodape -->

</div> <!-- Rodape Total -->




	
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-galeria-fancybox/jquery.fancybox.min.js"></script> 
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/swiper.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/pw-js/javascript.js"></script>
	<script type="text/javascript">
	 
		(function($) {	
			'use strict';
			//https://github.com/jlmakes/scrollreveal.js
			//[data-sr="enter direction"]
			//direction = top, bottom, right, left	
			window.sr = ScrollReveal();
			sr.reveal('.box-produtos a', { duration: 2000, origin: 'bottom', distance: '100px', viewFactor: 0.6 }, 100);			
		})();
		var swiper = new Swiper('.swiper-container', {
			pagination: '',
			slidesPerView: 1,
			slidesPerColumn: 1,
			paginationClickable: true,
			spaceBetween: 0,
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev',
			freeMode: false
    	});
		  
		  
	</script>
</body>
</html>