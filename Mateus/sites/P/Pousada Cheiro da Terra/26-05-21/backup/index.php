<?php
	$dadosPagina["titulo"]   = "Site Padrão Lançamento - Modelo 8";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Site Padrão Lançamento - Modelo 8\" />";
    $dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
    $dadosPagina["css"] = "<style></style>";
?>



    <div class="fundo-slider">
        <!-- Start WOWSlider.com BODY section -->
        <!-- add to the <body> of your page -->
        <div id="wowslider-container1">
            <div class="ws_images">
                <ul>
                    <li><img src="[template]/pw-slider-data/images/banner1.png" alt="1" title="1" id="wows1_0" /></li>
                    <li><img src="[template]/pw-slider-data/images/banner2.png" alt="2" title="2" id="wows1_1" /></li>
                    <li><img src="[template]/pw-slider-data/images/banner3.png" alt="1" title="1" id="wows1_2" /></li>
                    <li><img src="[template]/pw-slider-data/images/banner4.png" alt="2" title="2" id="wows1_3" /></li>
                </ul>
            </div>
            <div class="ws_shadow"></div>
        </div>
        
        <!-- End WOWSlider.com BODY section -->

        <div class="slogan-total">

            <div class="slogan">

                <div class="titulo">SEJA BEM-VINDO</div>
                <!-- Titulo -->
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </div>
                <!-- Conteudo -->

            </div>
            <!-- Slogan -->

        </div>
        <!-- Slogan Total -->

    </div>
    <!-- Fundo Slider -->



    <div class="box-01-total">

        <div class="box-01">

            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/index-01.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="titulo">Acomodações</div>
                    <!-- titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/index-02.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="titulo">Nosso Atelier</div>
                    <!-- titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/index-03.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="titulo">Descontos</div>
                    <!-- titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/index-04.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="titulo">Avaliações</div>
                    <!-- titulo -->
                    <div class="conteudo">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>

        </div>
        <!-- Box 01 -->

    </div>
    <!-- Box 01 Total -->



    <div class="box-02-total">

        <div class="box-02">

            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/fotos/index-01.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="conteudo">
                        <div class="titulo">FOTOS</div>
                        <!-- Titulo -->
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non tellus a, varius sodales nulla.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/fotos/index-02.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="conteudo">
                        <div class="titulo">CHALÉS</div>
                        <!-- Titulo -->
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non tellus a, varius sodales nulla.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/fotos/index-03.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="conteudo">
                        <div class="titulo">QUARTOS</div>
                        <!-- Titulo -->
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non tellus a, varius sodales nulla.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/fotos/index-04.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="conteudo">
                        <div class="titulo">CERÂMICA DE CUNHA</div>
                        <!-- Titulo -->
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non tellus a, varius sodales nulla.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/fotos/index-05.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="conteudo">
                        <div class="titulo">COMODIDADES</div>
                        <!-- Titulo -->
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non tellus a, varius sodales nulla.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>
            <a href="" title="">
                <div class="item">
                    <div class="img"><img src="[template]/pw-images/fotos/index-06.jpg" alt="" title="" /></div>
                    <!-- Img -->
                    <div class="conteudo">
                        <div class="titulo">CONTATO</div>
                        <!-- Titulo -->
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis massa, molestie non tellus a, varius sodales nulla.
                    </div>
                    <!-- Conteudo -->
                </div>
                <!-- Item -->
            </a>

        </div>
        <!-- Box 02 -->

    </div>
    <!-- Box 02 Total -->



    <div class="box-03-total">

        <div class="box-03">

            <div class="titulo">ENTRE EM CONTATO</div>
            <!-- titulo -->

            <form action="pw-form.php" method="post">

                <div class="box">

                    <div class="item">
                        <input name="campo[Nome]" type="text" placeholder="Nome:" />
                        <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                        <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
                    </div>
                    <!-- Item -->
                    <div class="item">
                        <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
                    </div>
                    <!-- Item -->

                </div>
                <!-- Box -->
                <input value="Enviar" class="submit" type="submit" style="width: 30%;" />

            </form>

        </div>
        <!-- Box 03 -->

    </div>
    <!-- Box 03 Total -->




    <div class="mapa">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3670.9792050573224!2d-44.967062384415634!3d-23.061223949199018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9d7d0049cb5e85%3A0x14137fd152b38a82!2sAlameda%20Francisco%20da%20Cunha%20Menezes%2C%201245%2C%20Cunha%20-%20SP%2C%2012530-000!5e0!3m2!1spt-BR!2sbr!4v1620416065081!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div>
    <!-- mapa -->
