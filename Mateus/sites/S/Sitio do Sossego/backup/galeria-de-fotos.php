<?php
	$dadosPagina["titulo"]   = "Galeria de Fotos";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
	$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
	$dadosPagina["css"] = "";
?>


<div class="conteudo-pages">

    <div class="titulo">
        <h1><img src="[template]/pw-images/logo-titulo.png" alt="" title=""> Galeria de Fotos</h1>
    </div>

    <div class="box-06-total">
        <div class="box-06">
            <div class="itens-total">

                <div class="gallery">

                    <a href="[template]/pw-images/galeria/alojamento/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/alojamento/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>alojamento</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/alojamento/grande/14.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery2">

                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/17.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/campo-de-futebol/pequena/3.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>campo de futebol</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/15.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/16.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/18.JPG"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/19.JPG"></a>
                    <!--<a href="[template]/pw-images/galeria/campo-de-futebol/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/campo-de-futebol/grande/14.jpg"></a>-->
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery3">

                    <a href="[template]/pw-images/galeria/churrasqueira/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/churrasqueira/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>churrasqueira</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/churrasqueira/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/churrasqueira/grande/3.JPG"></a>
                    <a href="[template]/pw-images/galeria/churrasqueira/grande/4.JPG"></a>
                    <a href="[template]/pw-images/galeria/churrasqueira/grande/5.JPG"></a>
                    <a href="[template]/pw-images/galeria/churrasqueira/grande/6.JPG"></a>
                    <a href="[template]/pw-images/galeria/churrasqueira/grande/7.JPG"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery4">

                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/cozinha-industrial/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>cozinha industrial</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/15.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/16.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/17.jpg"></a>
                    <a href="[template]/pw-images/galeria/cozinha-industrial/grande/18.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery5">

                    <a href="[template]/pw-images/galeria/dormitorio/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/dormitorio/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>dormitório</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/dormitorio/grande/2.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery6">

                    <a href="[template]/pw-images/galeria/igreja/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/igreja/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>igreja</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/igreja/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/15.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/16.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/17.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/18.jpg"></a>
                    <a href="[template]/pw-images/galeria/igreja/grande/19.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery7">

                    <a href="[template]/pw-images/galeria/lago/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/lago/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>lago</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/lago/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/lago/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/lago/grande/4.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery8">

                    <a href="[template]/pw-images/galeria/lazer/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/lazer/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>lazer</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/lazer/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/lazer/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/lazer/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/lazer/grande/5.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery9">

                    <a href="[template]/pw-images/galeria/meditacao/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/meditacao/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>meditação</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/meditacao/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/meditacao/grande/3.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery10">

                    <a href="[template]/pw-images/galeria/piscina/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/piscina/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>piscina</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/piscina/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/piscina/grande/9.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery11">

                    <a href="[template]/pw-images/galeria/playground/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/playground/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>playground</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/playground/grande/2.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery12">

                    <a href="[template]/pw-images/galeria/refeitorio/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/refeitorio/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>refeitório</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/refeitorio/grande/15.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery13">

                    <a href="[template]/pw-images/galeria/sala-de-estar/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/sala-de-estar/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>sala de estar</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/sala-de-estar/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/sala-de-estar/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/sala-de-estar/grande/4.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery14">

                    <a href="[template]/pw-images/galeria/visao-geral/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/visao-geral/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>visao geral</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/visao-geral/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/visao-geral/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/visao-geral/grande/4.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

                <div class="gallery15">

                    <a href="[template]/pw-images/galeria/w.c/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/w.c/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>w.c</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/w.c/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/w.c/grande/15.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery16">

                    <a href="[template]/pw-images/galeria/sorveteria/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/sorveteria/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>sorveteria</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/sorveteria/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/sorveteria/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/sorveteria/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/sorveteria/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/sorveteria/grande/6.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery17">

                    <a href="[template]/pw-images/galeria/tanque/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/tanque/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>tanque</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/tanque/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/15.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/16.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/17.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/18.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/19.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/20.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/21.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/22.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/23.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/24.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/25.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/26.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/27.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/28.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/29.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/30.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/31.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/32.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/33.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/34.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/35.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/36.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/37.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/38.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/39.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/40.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/41.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/42.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/43.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/44.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/45.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/46.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/47.jpg"></a>
                    <a href="[template]/pw-images/galeria/tanque/grande/48.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery18">

                    <a href="[template]/pw-images/galeria/frente/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/frente/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>frente</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/frente/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/frente/grande/11.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery19">

                    <a href="[template]/pw-images/galeria/balanco/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/balanco/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>balanço</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/balanco/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/balanco/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/balanco/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/balanco/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/balanco/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/balanco/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/balanco/grande/8.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery20">

                    <a href="[template]/pw-images/galeria/versiculos/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/versiculos/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>versiculos</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/versiculos/grande/8.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery21">

                    <a href="[template]/pw-images/galeria/estacionamento/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/estacionamento/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>estacionamento</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/estacionamento/grande/13.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery22">

                    <a href="[template]/pw-images/galeria/camping/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/camping/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>camping</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/camping/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/camping/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/camping/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/camping/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/camping/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/camping/grande/7.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery23">

                    <a href="[template]/pw-images/galeria/paisagens/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/paisagens/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>paisagens</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/11.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/12.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/13.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/14.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/15.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/16.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/17.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/18.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/19.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/20.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/21.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/22.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/23.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/24.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/25.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/26.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/27.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/28.jpg"></a>
                    <a href="[template]/pw-images/galeria/paisagens/grande/29.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->
                <div class="gallery24">

                    <a href="[template]/pw-images/galeria/tirolesa/grande/1.jpg">
                        <!--Grande-->
                        <div class="item">
                            <div class="img">
                                <img src="[template]/pw-images/galeria/tirolesa/pequena/1.jpg" title="" alt="">
                                <!--Pequena-->
                            </div><!-- Img -->
                            <div class="titulo">
                                <h3>tirolesa</h3>
                            </div><!-- Texto -->
                        </div><!-- Item -->
                    </a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/2.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/3.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/4.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/5.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/6.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/7.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/8.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/9.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/10.jpg"></a>
                    <a href="[template]/pw-images/galeria/tirolesa/grande/11.jpg"></a>
                    <!--Grande-->
                </div> <!-- gallery -->

            </div> <!-- itens-total -->
        </div> <!-- box-06 -->
    </div> <!-- box-06-total -->
</div> <!-- Conteudo Pages -->
