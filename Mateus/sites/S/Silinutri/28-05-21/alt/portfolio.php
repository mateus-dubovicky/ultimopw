<?php
$dadosPagina["titulo"]   = "Portfólio";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Portfólio\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">
    <h1>Portfólio</h1>
    
    <h2>Dra Silvana Emilia Jesus de Freitas</h2>

    <div class="texto-servico">
        <p>Dra Silvana Emilia Jesus de Freitas, CRN 3335,nutricionista,graduada em 1987, pela Faculdade de Ciências da Saúde São Camilo, atuando na área clinica desde então.</p>
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
    </div>

    <h2>Area Hospitalar</h2>

    <div class="texto-servico">
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
        <p>Especialização em administração de serviços de alimentação e nutrição pelo Instituto Central do Hospital das Clinicas, trabalhando nesse instituto até  1989.Depois atuei na área clínica do Hospital Israelita Albert Einstein, no atendimento a pacientes das unidades de moléstias infecciosas,  ortopedia, cardiologia, pacientes com câncer  e em tratamento de radioterapia, participei de vários projetos de educação nutricional , membro da Sociedade de Cardiologia do Estado de São  Paulo,  representando essa instituição até 1995.</p>
    </div>

    <h2>Docencia do Ensino Superrior</h2>

    <div class="texto-servico">
        
        <p>Atuação como docente da Faculdade das Ciências da Saúde, nas disciplinas de Técnica Dietética II, Diagnóstico em Nutrição,  Supervisora de estágios em Dietoterapia até 2000.</p>
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
    </div>

    <h2>Atendimento em Consultorio</h2>

    <div class="texto-servico">
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
        <p>Terapia nutricional em clínica geral de 1990 até 2005, em consultório próprio. Atendimento , de mulheres adultas, gestantes, nutrizes, mulheres na menopausa e pós menopausa na Clínica Célula Mater de 2004 a 2011.</p>
    </div>

    <h2>Personal diet</h2>

    <div class="texto-servico">
        <p>Atendimento de mulheres nas diversas etapas da vida, com ênfase na orientação da alimentação da família, mulheres que desejam engravidar,  gestantes, pós parto, nutrizes , nutrição em estética desde 2004 até o presente momento. Cursos de especialização em aleitamento materno, nutrição materno infantil .</p>
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
    </div>



</div>