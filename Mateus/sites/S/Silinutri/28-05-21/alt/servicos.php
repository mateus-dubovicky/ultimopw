<?php
$dadosPagina["titulo"]   = "Serviços";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"um teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Serviços\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "<style></style>";
?>

<div class="conteudo-pages">
    <h1>Serviços</h1>

    <h2>Avaliação Nutricional</h2>

    <div class="texto-servico">
        <p>histórico clínico e alimentar, avaliação física, diagnóstico através de exames bioquímicos, análise funcional.</p>
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
    </div>

    <h2>Confeção de Receitas e Cardápios</h2>

    <div class="texto-servico">
    <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
        <p>Orientação para confecção de receitas saudáveis e práticas a domicílio , para o próprio paciente, cozinheiros ou cuidadores. Planejamento de cardápios levando em consideração os hábitos alimentares da família, opções de refeições prontas frescas ou congeladas.</p>
        
    </div>

    <h2>Planejamento de Compras</h2>

    <div class="texto-servico">
        <p>Indicação de produtos quanto à sua qualidade e quantidade, interpretação dos rótulos de embalagens de produtos da despensa ou do supermercados, hortifrutigranjeiros.</p>
        <img src="[template]/pw-images/nutricao-clinica.jpg" alt="">
    </div>

</div>