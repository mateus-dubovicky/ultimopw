<?php
$dadosPagina["titulo"]   = "Vinhos da melhor importadora de vinhos do Brasil - SP";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Melhor empresa importadora de vinhos Serafins, vinhos e espumantes portugueses produzidos em várias regiões, Região do D´Ouro, rótulos exclusivos de qualidade.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Vinhos da melhor importadora de vinhos do Brasil - SP\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>


<div class="empresa-titulo1">
        <h1>NOSSOS VINHOS</h1>
</div>


    <div class="vinhos-total">

    <h2>Vinhos Brancos</h2>

        <div class="vinhos">

            <div class="vinho">
                 <a href="[url]/vinho-Dona-joia-Branco"> 
                    <img src="[template]/pw-images/vinho-Dona-joia-Branco.jpg" class="imgvinho1" alt="Vinho Branco" title="Vinho Branco">
                    <p>Dona Jóia Branco DOC Douro 2020</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Portugal</p> 
                 </a> 
            </div>

            <div class="vinho">
                 <a href="[url]/vinho-socalcos-penoja-branco"> 
                    <img src="[template]/pw-images/vinho-Socalcos-penajo-branco.jpg" alt="Vinho" title="Vinho">
                    <p>Socalcos da Penajóia Branco</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Portugal</p> 
                </a>  
            </div>

            
        </div>

        <h2>Vinhos Tintos</h2>

        <div class="vinhos dois">

            <div class="vinho">
                <!-- <a href="[url]/vinho"> -->
                    <img src="[template]/pw-images/vinho-100-segredos.png" alt="">
                    <p>100 Segredos <br> Tinto DOC Douro <br> 2019</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                <!-- </a> -->
            </div>

            <div class="vinho">
                <!-- <a href="[url]/vinho"> -->
                    <img src="[template]/pw-images/vinho-socalcos-de-penoja.png" alt="">
                    <p>Socalcos da Penajóia <br> Tinto </p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                <!-- </a> -->
            </div>

            <div class="vinho">
                <!-- <a href="[url]/vinho"> -->
                    <img src="[template]/pw-images/vinho-dona-joia-tinto.png" alt="">
                    <p>Dona Jóia <br> Tinto <br> DOC <br> Douro 2019</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                <!-- </a>    -->
            </div>

            <div class="vinho">
                <!-- <a href="[url]/vinho"> -->
                    <img src="[template]/pw-images/vinho-puzzle.png" alt="">
                    <p>Puzzle Signature <br> Tinto <br> Reserva DOC <br>  Douro 2018</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                <!-- </a>  -->
            </div>

            

        </div>


    </div>