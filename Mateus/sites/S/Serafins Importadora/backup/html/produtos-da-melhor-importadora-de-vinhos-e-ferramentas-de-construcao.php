<?php
	$dadosPagina["titulo"]   = "Produtos da melhor importadora de vinhos do Brasil - SP";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Melhor empresa importadora de vinhos Serafins, vinhos e espumantes portugueses produzidos em várias regiões, Região do D´Ouro, rótulos exclusivos de qualidade.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Produtos da melhor importadora de vinhos do Brasil - SP\" />";
	$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
	$dadosPagina["css"] = "";
?>


<div class="conteudo-pagesx">



    <div class="empresa-titulo1">
        <h1>PRODUTOS</h1>
    </div>

    <div class="caixay right">
        <div class="texto2">
            <div class="produtos-texto2">
                <h3><img src="[template]/pw-images/serafins-importadora-de-vinhos.png" alt="Serafins" title="Serafins">
                    <b>Vinhos e</b> espumantes
                </h3>
                <p>Contando com estreitos laços familiares em Portugal, a Serafins Importadora tem acesso ao que há de melhor em vinhos e espumantes portugueses produzidos em várias regiões, em especial na Região do D´Ouro. Oferecer ao brasileiro rótulos exclusivos de qualidade e de excelente custo benefício é nosso objetivo. <br><br>
                    Em breve divulgaremos nossos rótulos.
                </p>
            </div>
            <div class="imgproduto1">
                <img src="[template]/pw-images/vinhos-e-espumantes-serafins.jpg" alt="Vinhos e espumantes Serafins" title="Vinhos e espumantes Serafins"></div>
        </div>
    </div>

    <div class="bord"></div>

    <div class="logfort">
        <img src="[template]/pw-images/fortiga-ferramentas-construcao-civil.png" alt="Fortiga Ferramentas Construção Civil" title="Fortiga Ferramentas Construção Civil"></div>

    <div class="caixay left">
        <div class="texto2">
            <div class="imgproduto2 fort">
                <img src="[template]/pw-images/construcao-civil-serafins.jpg" alt="Construção Civil" title="Construção Civil"></div>
            <div class="produtos-texto2 forti">
                <h3>
                    <b>Ferramentas para</b><br>construção civil
                </h3>
                <p>Valendo-se do know-how em ferramentas para construção civil, a Serafins Importadora está trazendo ao mercado brasileiro sua linha Fortigra Ferramentas, oferecendo produtos dotados alta tecnologia e de um ótimo custo benefício.<br> Conheça nossa linha clicando aqui. <b><img src="[template]/pw-images/logo-fortiga-ferramentas.png" alt="Logo Fortiga Ferramentas" title="Logo Fortiga Ferramentas"></b>
                </p>
            </div>
        </div>
    </div>









</div>
