<?php
$dadosPagina["titulo"]   = "Vinho Branco Socalcos da Penója - Serafins Importadora";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Melhor empresa importadora de vinhos Serafins, vinhos e espumantes portugueses produzidos em várias regiões, Região do D´Ouro, rótulos exclusivos de qualidade.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Produtos da melhor importadora de vinhos do Brasil - SP\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>


<div class="empresa-titulo1">
        <h1>VINHO BRANCO</h1>
</div>

<div class="conteudo-vinho">

    <div class="conteudo-total">
        <div class="conteudo">

            <div class="vinho dois">
                <p class="titulo-vinho" > VINHO BRANCO  SOCALCOS DA PENAJÓIA  750 ml </p>
                <img src="[template]/pw-images/vinho-Socalcos-penajo-branco.jpg" class="vinhoDona-Branco" alt="Vinho Socalcos de Penoja Branco" title="Vinho Socalcos de Penoja Branco">
            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>REGIÃO</h4>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Douro, Portugal</p>
                </div>

                <div class="especificacao">
                    <h4>SAFRA</h4>
                    <p>NÃO ESPECIFICADA</p>
                </div>

            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>UVA</h4>
                    <img src="[template]/pw-images/uva.png" class="img-Bandeira" alt="Vinho de Uva" title="Vinho de Uva">
                    <p> UVAS VINÍFERAS EUROPÉIAS </p>
                </div>

                <div class="especificacao">
                    <h4>TEOR ALCÓOLICO %</h4>
                    <p>12</p>
                </div>

            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>TEMP. DE SERVIÇO</h4>
                    <i class="fas fa-thermometer-quarter"></i>
                    <p>6 a 10 ºc</p>
                </div>

                <div class="especificacao">
                    <h4> LONGEVIDADE </h4>
                    <i class="fas fa-hourglass-half"></i>
                    <p>1 a 3 anos</p>
                </div>

            </div>

            <div class="ficha-tecnica last">

                <div class="especificacao">
                    <h4>HARMONIZAÇÃO</h4>
                    <i class="fas fa-thermometer-quarter"></i>
                    <p class="especificacaop" >Ideal para saladas, entradas, peixes , frutos do mar, e carnes brancas.</p>
                </div>

                <div class="especificacao">
                    <h4>NOTAS DO SOMMELIER</h4>
                    <i class="fas fa-wine-glass-alt"></i>
                    <p>  Cor de palha Citrino. </p>
                    <p class="especificacaop" > Aromas complexos e frescos. <br> Corpo leve, ideal para o dia a dia.</p>
                </div>

            </div>









        </div>



    </div>
</div>