<?php
$dadosPagina["titulo"]   = "Vinho Tinto, Vinho Branco";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\" Vinho Tinto, Vinho Branco, Melhor empresa importadora de vinhos Serafins, vinhos e espumantes portugueses produzidos em várias regiões, Região do D´Ouro, rótulos exclusivos de qualidade.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Vinhos da melhor importadora de vinhos do Brasil - SP\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>


<div class="empresa-titulo1">
        <h1>NOSSOS VINHOS</h1>
</div>


    <div class="vinhos-total">

    <h2>Vinhos Brancos</h2>

        <div class="vinhos">

            <div class="vinho">
                 <a href="[url]/vinho-branco-Dona-Joia"> 
                    <img src="[template]/pw-images/vinho-Dona-joia-Branco.jpg" class="imgvinho1" alt="Vinho Dona Joia Branco" title="Vinho Dona Joia Branco">
                    <p>Dona Jóia Branco DOC Douro 2020</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Portugal</p> 
                 </a> 
            </div>

            <div class="vinho">
                 <a href="[url]/vinho-branco-Socalcos-Penoja"> 
                    <img src="[template]/pw-images/vinho-Socalcos-penajo-branco.jpg" alt="Vinho Socalcos de Penaja Branco" title="Vinho Socalcos de Penaja Branco">
                    <p>Socalcos da Penajóia Branco</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Portugal</p> 
                </a>  
            </div>

            
        </div>

        <h2>Vinhos Tintos</h2>

        <div class="vinhos dois">

            <div class="vinho">
                 <a href="[url]/vinho-tinto-100-segredos"> 
                    <img src="[template]/pw-images/vinho-100-segredos.png" alt="Vinho 100 Segredos Tinto" title="Vinho 100 Segredos Tinto"> 
                    <p>100 Segredos <br> Tinto DOC Douro <br> 2019</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                 </a> 
            </div>

            <div class="vinho">
                <a href="[url]/vinho-tinto-socalcos-penoja"> 
                    <img src="[template]/pw-images/vinho-socalcos-de-penoja.png" alt="Vinho Socalcos de Penoja Tinto" title="Vinho Socalcos de Penoja Tinto"> 
                    <p>Socalcos da Penajóia <br> Tinto </p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                </a> 
            </div>

            <div class="vinho">
                 <a href="[url]/vinho-tinto-Dona-Joia"> 
                    <img src="[template]/pw-images/vinho-dona-joia-tinto.png" alt="Vinho Dona Joia Tinto" title="Vinho Dona Joia Tinto"> 
                    <p>Dona Jóia <br> Tinto <br> DOC <br> Douro 2019</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                 </a>    
            </div>

            <div class="vinho">
                 <a href="[url]/vinho-tinto-Puzzle-Signature"> 
                    <img src="[template]/pw-images/vinho-puzzle.png" alt="Vinho Puzzle Signature Tinto" title="Vinho Puzzle Signature Tinto">
                    <p>Puzzle Signature <br> Tinto <br> Reserva DOC <br>  Douro 2018</p>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <h4>Portugal</h4> 
                 </a>  
            </div>

            

        </div>


    </div>