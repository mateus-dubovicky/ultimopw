<?php
$dadosPagina["titulo"]   = "Vinho Tinto Puzzle Signature - Serafins Importadora";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Melhor empresa importadora de vinhos Serafins, vinhos e espumantes portugueses produzidos em várias regiões, Região do D´Ouro, rótulos exclusivos de qualidade.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Produtos da melhor importadora de vinhos do Brasil - SP\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>


<div class="empresa-titulo1">
        <h1>VINHO TINTO</h1>
</div>

<div class="conteudo-vinho">

    <div class="conteudo-total">
        <div class="conteudo">

            <div class="vinho dois">
                <p class="titulo-vinho" > VINHO TINTO PUZZLE SIGNATURE  DOC DOURO 2018  750 ml </p>
                <img src="[template]/pw-images/vinho-puzzle.png" class="vinhoDona-Branco" alt="Vinho Tinto Puzzle Signature" title="Vinho Tinto Puzzle Signature">
            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>REGIÃO</h4>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Douro, Portugal</p>
                </div>

                <div class="especificacao">
                    <h4>SAFRA</h4>
                    <p>2018</p>
                </div>

            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>UVA</h4>
                    <img src="[template]/pw-images/uva.png" class="img-Bandeira" alt="Vinho de Uva" title="Vinho de Uva">
                    <p>  Touriga Franca , Touriga Nacional </p>
                </div>

                <div class="especificacao">
                    <h4>TEOR ALCÓOLICO %</h4>
                    <p>14</p>
                </div>

            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>TEMP. DE SERVIÇO</h4>
                    <i class="fas fa-thermometer-quarter"></i>
                    <p>16 a 18 ºc</p>
                </div>

                <div class="especificacao">
                    <h4> LONGEVIDADE </h4>
                    <i class="fas fa-hourglass-half"></i>
                    <p>10 ANOS</p>
                </div>

            </div>

            <div class="ficha-tecnica last">

                <div class="especificacao">
                    <h4>HARMONIZAÇÃO</h4>
                    <i class="fas fa-thermometer-quarter"></i>
                    <p class="especificacaop" >  Ideal para queijos, massas e carnes vermelhas.</p>
                </div>

                <div class="especificacao">
                    <h4>NOTAS DO SOMMELIER</h4>
                    <i class="fas fa-wine-glass-alt"></i>
                    <p>  Cor Rubi intensa. </p>
                    <p class="especificacaop" >  Aromas concentrados de frutas vermelhas, geléias e frutas secas. <br> Sabor estruturado, taninos suaves e final elegante.</p>
                </div>

            </div>


        </div>

    </div>
</div>