<?php
$dadosPagina["titulo"]   = "Vinho Branco Dona Joia - Serafins Importadora";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Melhor empresa importadora de vinhos Serafins, vinhos e espumantes portugueses produzidos em várias regiões, Região do D´Ouro, rótulos exclusivos de qualidade.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Produtos da melhor importadora de vinhos do Brasil - SP\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>

<div class="empresa-titulo1">
        <h1>VINHO BRANCO</h1>
</div>

<div class="conteudo-vinho">

    <div class="conteudo-total">
        <div class="conteudo">

            <div class="vinho dois">
                <p class="titulo-vinho" >VINHO BRANCO DONA JÓIA DOC DOURO 2020  750 ml</p>
                <img src="[template]/pw-images/vinho-Dona-joia-Branco.jpg" class="vinhoDona-Branco" alt="Vinho Dona Joia Branco" title="Vinho Dona Joia Branco">
            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>REGIÃO</h4>
                    <img src="[template]/pw-images/bandeira_de_Portugal.png" class="img-Bandeira" alt="Vinho de Portugal" title="Vinho de Portugal">
                    <p>Douro, Portugal</p>
                </div>

                <div class="especificacao">
                    <h4>SAFRA</h4>
                    <p>2020</p>
                </div>

            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>UVA</h4>
                    <img src="[template]/pw-images/uva.png" class="img-Bandeira" alt="Vinho de Uva" title="Vinho de Uva">
                    <p>Malvasia Fina , Fernão Pires e Viosinho</p>
                </div>

                <div class="especificacao">
                    <h4>TEOR ALCÓOLICO %</h4>
                    <p>13</p>
                </div>

            </div>

            <div class="ficha-tecnica">

                <div class="especificacao">
                    <h4>TEMP. DE SERVIÇO</h4>
                    <img src="[template]/pw-images/vinho-termometro-servico.png" class="img-Bandeira" alt="Vinho Temperatura de Serviço" title="Vinho Temperatura de Serviço">
                    <p>6 a 10 ºc</p>
                </div>

                <div class="especificacao">
                    <h4> LONGEVIDADE </h4>
                    <img src="[template]/pw-images/vinho-longevidade.png" class="img-Bandeira" alt="Longevidade do Vinho" title="Longevidade do Vinho">
                    <p>1 a 3 anos</p>
                </div>

            </div>

            <div class="ficha-tecnica last">

                <div class="especificacao">
                    <h4>HARMONIZAÇÃO</h4>
                    <img src="[template]/pw-images/vinho-harmonizacao.png" class="img-Bandeira" alt="Vinho Harmonização" title="Vinho Harmonização">
                    <p class="especificacaop" >Ideal para saladas, entradas, peixes, frutos do mar e carnes brancas.</p>
                </div>

                <div class="especificacao">
                    <h4>NOTAS DO SOMMELIER</h4>
                    <img src="[template]/pw-images/vinho-sommelier.png" class="img-Bandeira" alt="Sommelier de Vinho Nota " title="Sommelier de Vinho Notas">
                    <p> Cor Citrina. </p>
                    <p class="especificacaop" > Aroma Floral delicado e com notas de frutas maduras. <br> Sabor persistente, boa acidez e presença de mineralidade.</p>
                </div>

            </div>


        </div>



    </div>
</div>