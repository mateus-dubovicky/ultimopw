<?php
	$dadosPagina["titulo"]   = "Dicas de Segurança";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Dicas de Segurança, Vigilancia, Portaria, Seguranca Eletrônica, Seguranca Pessoal, Seguranca Privada, Camera Residencial.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Dicas de Segurança\" />";
	$dadosPagina["metas"][2] = "";
	$dadosPagina["css"] = "";
?>

<div class="conteudo-pages">
    <h1>DICAS DE SEGURANÇA</h1>
    <div class="empresa-total">
        <div class="texto-empresa serv">

           
            <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> Dicas de especialistas, ilustradas de uma forma divertida</h2>
                    <p>
                    A observação de regras simples de segurança pode evitar assaltos, roubos e outros tipos de delitos.
                    </p>
                    <p>
                    Pensando nisso o <strong>GRUPO SECRUZ</strong> desenvolveu um manual que fornece dicas básicas de segurança, baseadas no conhecimento dos seus especialistas para que os cidadãos tomem os cuidados necessários.
                   
                    </p>
                     <p>
                  
                </div>         
            
         
            </div>
            
             <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> EVITE ASSALTOS EM SUA RESIDÊNCIA</h2>
                    <p>
                   Ao viajar nunca deixe as luzes acesas, cancele a entrega de revistas e jornais, este é o sinal que o bandido precisa para saber durante o dia, que não tem ninguém na residência.
                    </p>
                     <p>
                   Instale sistemas de segurança como CFTV, Cerca Elétrica e Alarmes monitorados por uma empresa especializada na área de segurança.
                    </p>
                    <p>
                   Contrate uma empresa de segurança com profissionais treinados e supervisionados para proteger o seu patrimônio.
                    </p>
                   <p>
                   Evite ficar de bate papo com amigos e parentes na calçada em frente à sua casa, pois facilmente o meliante poderia abordá-los para entrar na residência e concretizar o assalto.
                    </p> 
                    <p>
                   Ao chegar do trabalho ou de um passeio de carro, antes de entrar em casa verifique se há pessoas estranhas, ou com atitudes suspeitas, pois esta é a oportunidade que o bandido precisa para fazer uma abordagem e entrar em sua residência.
                    </p>
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/seguranca-residencial.jpg" alt="Segurança Residencial" title="Segurança Residencial" ></div>
            </div>
             
             
               <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> SEGURANÇA PESSOAL NAS RUAS</h2>
                    <p>
                   Se alguém vier lhe pedir qualquer tipo de informação, atente-se se o mesmo não está acompanhado de outra pessoa, este método é usado para mantê-lo distraído para que o 2º elemento furte os seus pertences, sem que você perceba a manobra desta pratica de crime.
                    </p>
                     <p>
                   Procure sempre andar nas ruas mantendo distância das pessoas a sua frente e a sua retaguarda, nunca ande distraído, isto facilita a ação dos meliantes.
                    </p>
                    <p>
                   Quando for buscar alguém da família em escolas, supermercados ou shopping centers, procure não ficar dentro do veículo aguardando a pessoa. Estacione o veículo e fique próximo a entrada destes locais para evitar chamar atenção de meliantes que tenham a intenção de fazer o sequestro.
                    </p>
                   <p>
                   Nunca use suas sacolas e mochilas nas costas, esta atitude facilita a ação dos meliantes que usam geralmente estiletes, facas ou canivetes, para subtrair os seus pertences.
                    </p> 
                    <p>
                   Ao fazer compras, evite expor a sua carteira, pois se neste momento estiver pelo local algum meliante ou olheiros deste meliante, estas informações serão passadas para que você seja assaltado ao sair, evite manter uma grande quantidade de dinheiro na carteira, para não chamar a atenção destas pessoas, procure fazer suas compras usando o cartão dos bancos.
                    </p>
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/porteiro.jpg" alt="Porteiro" title="Porteiro" ></div>
            </div>
            
            
               <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> COMO EVITAR ASSALTOS EM CONDOMÍNIOS</h2>
                    <p>
                   É fundamental que sejam criadas Normas e Procedimentos, as quais deverão ser do conhecimento de todos os condôminos, pois deverão cumprir as Normas e orientar seus visitantes e empregados domésticos a também cumprirem fielmente todas as Normas.
                    </p>
                     <p>
                   Os Porteiros deverão ser treinados e constantemente orientados a seguirem fielmente todas as Normas e Procedimentos, bem como cobrar de todos os moradores o cumprimento das Normas.
                    </p>
                    <p>
                   Quando o porteiro anunciar alguma visita, o morador deve certificar-se que se trata da pessoa anunciada, fazendo o reconhecimento através do sistema de CFTV interno, antes de liberar a entrada do visitante. Alguns assaltantes se fazem passar por parentes ou amigos de condôminos, para terem o acesso liberado na portaria e posteriormente no apartamento.
                    </p>
                   <p>
                   O morador deve evitar deixar a cópia das chaves do apartamento com empregados domésticos, também não é aconselhável deixar as chaves na portaria para ser entregue aos empregados domésticos. Cópias das chaves podem ser tiradas, com a finalidade utilizá-las quando o morador viajar.
                    </p> 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/vigilante.jpg" alt="Vigilante" title="Vigilante"></div>
            </div>
            
            
             <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> COMO EVITAR SER ENGANADO POR VIGARISTAS</h2>
                    <p>
                   Se for vender algum bem e lhe oferecerem um valor maior do que ele vale, ou prontamente aceitarem o valor pedido sem tentar uma negociação, é prudente pesquisar a idoneidade do comprador, antes de fechar o negócio.
                    </p>
                     <p>
                   Se o seu veículo quebrar na rua sem um motivo aparente e logo em seguida chegar um mecânico que por acaso estava passando pelo local, desconfie e não aceite o serviço, é mais prudente e seguro chamar a seguradora, que enviará um mecânico credenciado para resolver o problema.
                    </p>
                    <p>
                   Se durante uma fiscalização o fiscal for muito rigoroso, mas logo em seguida se propor a quebrar o galho e pedir alguma vantagem, desconfie, possivelmente trata-se de um vigarista fazendo-se passar por fiscal.
                    </p>
                   <p>
                   O morador deve evitar deixar a cópia das chaves do apartamento com empregados domésticos, também não é aconselhável deixar as chaves na portaria para ser entregue aos empregados domésticos. Cópias das chaves podem ser tiradas, com a finalidade utilizá-las quando o morador viajar.
                    </p> 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/portaria.jpg" alt="Portaria" title="Portaria" ></div>
            </div>
            
            
            <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> CUIDADOS PARA EVITAR SEQÜESTROS</h2>
                    <p>
                   Se for caminhar, prefira fazê-lo em locais movimentados como, por exemplo, parques e clubes, nestes locais é mais difícil a ação de meliantes, se este crime for acontecer em seu deslocamento a pé.
                    </p>
                     <p>
                   Sempre tenha atitudes preventivas.
                    </p>
                    <p>
                   Quando for buscar alguém da família em escolas, supermercados ou shopping centers, procure não ficar dentro do veículo aguardando a pessoa. Estacione o veículo e fique próximo a entrada destes locais, para evitar chamar atenção de meliantes que tenham a intenção de fazer o sequestro.
                    </p>
                   <p>
                  Evite as rotinas como itinerário para ir trabalhar ou buscar suas crianças, tenha alternativas de roteiro, pois esta atitude confundirá o meliante num planejamento de sequestro.
                    </p> 
                    <p>
                  Ao chegar do trabalho ou de um passeio de carro, antes de entrar em casa, verifique se há pessoas estranhas, ou com atitudes suspeitas, pois esta é a oportunidade que o bandido precisa para fazer uma abordagem e entrar em sua residência.
                    </p> 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/flanelinha.jpg" alt="Flanelinha" title="Flanelinha" ></div>
            </div>
            
            
            <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> CONSELHOS NA HORA DE VIAJAR</h2>
                    <p>
                   Não comente a viagem próximo de pessoas estranhas, mas avise os parentes e os vizinhos de confiança e solicite que recolham as correspondências e jornais, também solicite que seja feita eventualmente a varrição de folhas na calçada e na frente da residência, para não dar indícios de que os moradores não estão na casa.
                    </p>
                     <p>
                   Quando desembarcar no seu destino, ou no retorno para casa, utilize serviços de taxi de empresas especializadas e somente carros que estejam nos pontos oficiais de taxi.
                    </p>
                 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/jardineiro.jpg" alt="Jardineiro" title="Jardineiro" ></div>
            </div>
            
            
                  <div class="item-text serv">

                <div class="item">
                
                    <h2><i class="fas fa-angle-double-right"></i> CUIDADOS COM AS CRIANÇAS</h2>
                    <p>
                   A melhor maneira para evitar atitudes de criminosos com as crianças é a educação no lar, oriente e mostre a elas os perigos, não adianta esconder o que está acontecendo, temos de prepará-las para a realidade.
                    </p>
                     <p>
                   Sempre que forem sair de suas casas, que estejam acompanhadas de uma pessoa responsável.
                    </p>
                    <p>
                   Sempre questione o lugar que a criança está indo, peça informações sobre o local a pessoas conhecidas, por meio de internet, através de telefone, ligue e questione, procure conhecer o ambiente que o seu filho irá frequentar.
                    </p>
                   <p>
                  Nunca aceite presentes de pessoas estranhas, esta é uma das artimanhas usadas pelos aliciadores de menores e pedófilos, o que facilita a ação dessas pessoas mal intencionadas.
                    </p> 
                    <p>
                  Mostre às crianças reportagens, artigo de jornais, comente os casos mais recentes sobre a ação dos criminosos, nossas crianças passaram a ter uma infância mais curta na atualidade, elas têm de amadurecer mais cedo e estar preparadas para o mundo em que vivemos, pois se isto não acontecer, elas serão um alvo muito fácil para as pessoas de má índole.
                    </p>
                     <p>
                  Quando estiver seguindo para a sua residência, verifique se não está sendo seguido por outro carro ou moto e quando chegar em casa, antes de entrar verifique se nas proximidades existe algum vendedor ambulante, movimentação de pessoas suspeitas, ou veículo parado próximo do portão da garagem com pessoas dentro, se notar alguma destas situações, é prudente dar uma volta no quarteirão antes de entrar na garagem.
                    </p> 
                     <p>
                  Sempre que parar o seu veículo em semáforos procure manter distância do veículo a sua frente, no caso de um assalto você poderá ter a oportunidade de sair do local.
                    </p> 
                     <p>
                  Quando suspeitar que está sendo seguido por pessoas de carro ou motos, pare em locais de grandes movimentos, peça ajuda a polícia, até que estas pessoas se evadam do local.
                    </p> 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/profissionais-vigilancia.jpg" alt="Profissionais de Vigilancia" title="Profissionais de Vigilancia" ></div>
            </div>
            
            
                  <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> EVITE ASSALTOS A AUTOMÓVEIS</h2>
                    <p>
                   Quando estiver se aproximando de um cruzamento e o farol estiver fechado, reduza a velocidade com bastante antecedência e procure manter o carro em movimento o maior tempo possível, na grande maioria das ocorrências os carros parados são os alvos dos assaltantes.
                    </p>
                     <p>
                   Use sempre estacionamentos que tenham seguro.
                    </p>
                    <p>
                  Sempre deixe o seu veículo pela pista central ou pelo lado esquerdo, esta atitude facilita no caso de uma evasão e ao parar, observe sempre os retrovisores direito e esquerdo, para ver alguma atitude suspeita, mas se for abordado por algum meliante nunca reaja, mantenha as mãos ao volante para o meliante ter a certeza de que você não está armado e saia do veiculo calmamente, lembre-se o maior bem é a sua vida.
                    </p>
                   <p>
                  Quando estiver dirigindo o veículo, mantenha as portas trancadas e as janelas sempre fechadas, não fale no celular e evite levantar o braço em que utiliza o relógio, quando pedestres ou vendedores estiverem próximos do carro.
                    </p> 
                    <p>
                  Nunca deixe o seu veículo estacionado em ruas de pouco movimento, esta atitude facilita a ação de ladrões.
                    </p>
                     <p>
                  Quando estiver seguindo para a sua residência, verifique se não está sendo seguido por outro carro ou moto e quando chegar em casa, antes de entrar verifique se nas proximidades existe algum vendedor ambulante, movimentação de pessoas suspeitas, ou veículo parado próximo do portão da garagem com pessoas dentro, se notar alguma destas situações, é prudente dar uma volta no quarteirão antes de entrar na garagem.
                    </p> 
                     <p>
                  Sempre que parar o seu veículo em semáforos procure manter distância do veículo a sua frente, no caso de um assalto você poderá ter a oportunidade de sair do local.
                    </p> 
                     <p>
                  Quando suspeitar que está sendo seguido por pessoas de carro ou motos, pare em locais de grandes movimentos, peça ajuda a polícia, até que estas pessoas se evadam do local.
                    </p> 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/seguranca-patrimonial.jpg" alt="Seguranca Patrimonial" title="Seguranca Patrimonial"></div>
            </div>
            
              <div class="item-text serv">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> CUIDADOS COM CARTÕES E SAQUES</h2>
                    <p>
                   Ao efetuar o pagamento com cartão de crédito, evite que o cartão fique longe de sua vista, confira o valor da compra antes de assinar e solicite sempre sua via do comprovante de venda. Confira também se o cartão devolvido realmente é o seu cartão.
                    </p>
                     <p>
                  Quando estiver no caixa eletrônico e apresentar erro no cartão, não aceite ajuda de pessoas que estejam na fila, eles se aproveitam da situação para memorizar a senha e trocar o cartão sem que a vítima perceba.
                    </p>
                    <p>
                  Os saques com cartões devem ser feitos preferencialmente durante o dia e em caixas eletrônicos instalados em locais de grande movimentação de pessoas, mas quando não houver outra alternativa e o saque ocorrer durante a noite, leve um acompanhante. Oriente o acompanhante a não permanecer no carro, o acompanhante deverá ir até o caixa eletrônico e ficar na fila, como se também fosse sacar o dinheiro.
                    </p>
                   <p>
                  Nunca aceite presentes de pessoas estranhas, esta é uma das artimanhas usadas pelos aliciadores de menores e pedófilos, o que facilita a ação dessas pessoas mal intencionadas.
                    </p> 
                    <p>
                  Mostre às crianças reportagens, artigo de jornais, comente os casos mais recentes sobre a ação dos criminosos, nossas crianças passaram a ter uma infância mais curta na atualidade, elas têm de amadurecer mais cedo e estar preparadas para o mundo em que vivemos, pois se isto não acontecer, elas serão um alvo muito fácil para as pessoas de má índole.
                    </p>
                     <p>
                  Quando estiver seguindo para a sua residência, verifique se não está sendo seguido por outro carro ou moto e quando chegar em casa, antes de entrar verifique se nas proximidades existe algum vendedor ambulante, movimentação de pessoas suspeitas, ou veículo parado próximo do portão da garagem com pessoas dentro, se notar alguma destas situações, é prudente dar uma volta no quarteirão antes de entrar na garagem.
                    </p> 
                     <p>
                  Sempre que parar o seu veículo em semáforos procure manter distância do veículo a sua frente, no caso de um assalto você poderá ter a oportunidade de sair do local.
                    </p> 
                     <p>
                  Quando suspeitar que está sendo seguido por pessoas de carro ou motos, pare em locais de grandes movimentos, peça ajuda a polícia, até que estas pessoas se evadam do local.
                    </p> 
                   
                  
                
                </div>         
                      
               <div class="item"><img src="[template]/pw-images/portaria-virtual.jpg" alt="Portaria Virtual" title="Portaria Virtual" ></div>
            </div>
            
            
            
        </div>


    </div>
 
</div>

