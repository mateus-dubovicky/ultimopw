<?php
	$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
	$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
	$dadosPagina["css"] = "";
?>

<div class="conteudo-pages">
    <h1>SEGURANÇA EM EVENTOS</h1>
    <div class="empresa-total">
        <div class="texto-empresa">

           
            <div class="item-text seguranca">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> GRUPO SECRUZ.</h2>
                    <p>
                     <strong>O GRUPO SECRUZ </strong>ossui uma equipe formada por profissionais de vigilância, treinados e preparados para atuar na segurança dos mais variados eventos, sejam públicos ou privados e tem como premissa, desenvolver planejamentos estratégicos e soluções customizadas que atendam as necessidades dos mais variados clientes no ramo de eventos.
                    </p>
                    <p>
                      O objetivo da nossa equipe de segurança nos eventos é garantir a tranquilidade no controle de acesso e durante a permanência do público e convidados nos locais escolhidos para os eventos, atuando de forma íntegra, eficaz e transparente, mantendo a equipe sempre atualizada e capacitada para assessorar e organizar a segurança, desde eventos de pequeno e médio porte até a realização de grandes convenções, congressos, feiras e eventos em geral. Os serviços podem ser contratados para eventos em Guarulhos e em todo o estado de São Paulo.

                    </p>
                </div>
          
           
              <div class="item"><img src="[template]/pw-images/segurança-em-eventos.jpg" alt=""></div>

            </div>
        </div>


    </div>
 
</div>

