<?php
	$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
	$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
	$dadosPagina["css"] = "";
?>

	<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container1">
	<div class="ws_images"><ul>
		<li><img src="[template]/pw-slider-data/images/1.jpg" alt="1" title="1" id="wows1_0"/></li>
		<li><img src="[template]/pw-slider-data/images/2.jpg" alt="2" title="2" id="wows1_1"/></li>
	</ul></div>

	<div class="ws_shadow"></div>
	</div>	
	<!-- End WOWSlider.com BODY section -->


    <div class="box-01-total">

        <a class="entrar" href="http://oprojetoweb.com.br/provas/grupo-secruz-programado1/empresa" title="" data-icon="empresa">
            <div class="box-01">

                <div class="conteudo">
                    <div class="titulo">BEM-VINDO AO<br />NOSSO SITE</div> <!-- Titulo -->
                    <div class="linha">
                        <div class="barra"></div>
                    </div> <!-- Linha -->
                    <div class="texto">
                     Foi fundada por profissionais que atuam há mais de 30 anos na área de Portaria limpeza e Conservação. Oferecemos serviços personalizados, feitos sob medida para as necessidades de nossos contratantes.
                    
                    </div> <!-- Texto -->
                    <div class="entrar">Saiba mais</div> <!-- Entrar -->
                </div> <!-- Conteudo -->
                <div class="img">
                    <img src="[template]/pw-images/box-01.jpg" alt="" title="" />
                    <div class="info"><i class="far fa-thumbs-up"></i><br />"Atender Bem Para Atender Sempre"</div> <!-- Info -->
                </div> <!-- Img -->

            </div> <!-- Box 01 -->
        </a>

    </div> <!-- conteudo index -->


    <div class="box-02-total">

        <div class="box-02">

            <a class="conteudo" href="http://oprojetoweb.com.br/provas/grupo-secruz-programado1/seguranca-eletronica" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-01.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">SEGURANÇA ELETRÔNICA</div> <!-- Titulo -->
                    <div class="conteudo">
                     Mais que oferecer serviços terceirizados, trabalhamos para levar tranquilidade aos nossos parceiros, há mais de 05 anos no mercado, reunimos clientes nos segmentos: empresas privadas órgãos públicos, shopping, faculdades, transportadoras, hospitalar, industrial, comercial, educacional, entre outros
                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>
            <a class="conteudo" href="http://oprojetoweb.com.br/provas/grupo-secruz-programado1/seguranca-em-eventos" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-02.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">SEGURANÇA EM EVENTOS</div> <!-- Titulo -->
                    <div class="conteudo">
                     O GRUPO SECRUZ possui uma equipe formada por profissionais de vigilância, treinados e preparados para atuar na segurança dos mais variados eventos, sejam públicos ou privados e tem como premissa, desenvolver planejamentos estratégicos e soluções customizadas que atendam as necessidades dos mais variados clientes no ramo de eventos.
                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>
            <a class="conteudo" href="http://oprojetoweb.com.br/provas/grupo-secruz-programado1/portaria-e-manobrista" title="" data-icon="empresa">
                <div class="item">

                    <div class="img"><img src="[template]/pw-images/box-02-03.jpg" alt="" title="" /></div> <!-- Img -->
                    <div class="titulo">PORTARIA E MANOBRISTA</div> <!-- Titulo -->
                    <div class="conteudo">
                    O serviço de Portaria e Manobrista GRUPO SECRUZ dispõe de profissionais empenhados em oferecer zelo e organização ao estacionamento do estabelecimento comercial.

                    </div> <!-- Conteudo -->
                    <div class="entrar">Saiba Mais</div> <!-- Entrar -->

                </div> <!-- Item -->
            </a>

        </div> <!-- BOx 02 -->

    </div> <!-- Box 02 Total -->

    <div class="box-03-total">

        <div class="box-03">

            <div class="linha">
                <div class="barra"></div>
            </div> <!-- Linha -->
            <div class="titulo">ENTRE EM <br />CONTATO</div> <!-- Titulo -->

            <form action="mail-contato.php" method="post">
                <div class="item">
                    <input name="campo[Nome]" type="text" placeholder="Nome:" />
                    <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                    <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
                </div> <!-- item -->
                <div class="item">
                    <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
                </div> <!-- item -->
                <input class="submit" type="submit" value="Enviar" />
            </form>

        </div> <!-- Box 03 -->

    </div> <!-- Conteudo Pages -->

    <div class="mapa">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d731.585222436976!2d-46.39249661995134!3d-23.404186164539016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce8848a2c9cec9%3A0x3e1f58e13b6a0cbb!2sR.%20Bias%20Fortes%2C%20174%20-%20Vila%20Nova%20Bonsucesso%2C%20Guarulhos%20-%20SP%2C%2007176-300!5e0!3m2!1spt-BR!2sbr!4v1608749703556!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div> <!-- mapa -->
