<?php
	$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
	$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
	$dadosPagina["css"] = "";
?>

<div class="conteudo-pages">
    <h1>EMPRESA</h1>
    <div class="empresa-total">
        <div class="texto-empresa">

            <img src="[template]/pw-images/grupo-secruz-servicos.jpg" alt="">

            <div class="item-text">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> QUEM SOMOS</h2>
                    <p>
                     Seja qual for a sua demanda em serviços terceirizados, nós temos a melhor solução para você. Contamos com um time de profissionais comprometidos, treinados para proteger e cuidar de pessoas, empresas, corporações e residências. Terceirize com a gente e fique tranquilo 

                    </p>
                </div>
          
                 <div class="item list">
                    <h2><i class="fas fa-angle-double-right"></i> SERVIÇOS DIFERENCIADOS PARA SUA EMPRESA</h2>
                   <ul>
                      
                      <li> Portaria</li>
                      <li> Asseio e conservação</li>
                      <li> Auxiliar de Serviços Gerais</li>
                      <li> Vigia</li>
                      <li> Manutenção Geral de Condomínios</li>
                      <li> Limpeza Eventual</li>
                      <li> Diarista</li>
                      <li> Jardinagem</li>

                   </ul>
                </div>
             

            </div>
        </div>


    </div>
    <div class="mvv-total">
        <div class="mvv">

            <div class="item">
                <div class="icone"><i class="fas fa-location-arrow"></i></div>
                <h3>Objetivo</h3>
                <p>  Crescer de forma gradativa, consecutiva e precisa, garantindo solidez de nossos negócios, como também dos nossos clientes e parceiros.</p>
            </div>

            <div class="item empresa">
                <div class="icone"><i class="fas fa-eye"></i></div>
                <h3>Nível de instrução acima da média do mercado</h3>
                     <ul>
                      
                      <li> Avaliação constante de desempenho.</li>
                      <li> Treinamento especificado para cada cliente.</li>
                      <li> Seleção criteriosa e rigorosa de candidatos.</li>
                  

                   </ul>
            </div>

    

        </div>
    </div>
</div>

