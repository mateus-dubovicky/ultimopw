<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
$dadosPagina["metas"][2] = "<link rel='stylesheet' type='text/css' href='template/pw-slider-engine/style.css' />";
$dadosPagina["css"] = "";
?>

<!-- Start WOWSlider.com BODY section -->
<!-- add to the <body> of your page -->
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <li><img src="[template]/pw-slider-data/images/1.jpg" alt="1" title="1" id="wows1_0" /></li>
            <li><img src="[template]/pw-slider-data/images/2.jpg" alt="2" title="2" id="wows1_1" /></li>
            <li><img src="[template]/pw-slider-data/images/3.jpg" alt="2" title="2" id="wows1_1" /></li>
            <li><img src="[template]/pw-slider-data/images/4.jpg" alt="2" title="2" id="wows1_1" /></li>
        </ul>
    </div>

    <div class="ws_shadow"></div>
</div>
<!-- End WOWSlider.com BODY section -->

<div class="box-01-total">

    <a class="entrar" href="[url]/empresa" title="" data-icon="empresa">
        <div class="box-01">

            <div class="conteudo">
                <div class="titulo">BEM-VINDO AO<br />NOSSO SITE</div>
                <div class="linha">
                    <div class="barra"></div>
                </div>
                <div class="texto">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu, at suscipit augue egestas sollicitudin. Aliquam et elit erat. Quisque aliquet placerat nisl, a semper sapien gravida in. Nunc non felis aliquet, luctus lacus nec, gravida ipsum. Mauris interdum pulvinar diam, sit amet ultricies ligula condimentum pharetra.
                </div>
                <div class="entrar">Saiba mais</div>
            </div>
            <div class="img">
                <img src="[template]/pw-images/box-01.jpg" alt="" title="" />
                <div class="info"><i class="far fa-thumbs-up"></i><br />Competência e técnica UNIdas a uma escuta acolhedora e centrada no bem-estar da família e do paciente.</div>
            </div>

        </div>
    </a>

</div>

<div class="box-02-total">

    <div class="box-02">

        <a class="conteudo" href="" title="" data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/box-02-01.jpg" alt="" title="" /></div>
                <div class="titulo">Fonoaudiologia</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu,
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>
        <a class="conteudo" href="" title="" data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/box-02-02.jpg" alt="" title="" /></div>
                <div class="titulo">Fisioterapia</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu,
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>
        <a class="conteudo" href="" title="" data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/box-02-03.jpg" alt="" title="" /></div>
                <div class="titulo">Pedagogia</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu,
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>

        <a class="conteudo" href="" title="" data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/box-02-04.jpg" alt="" title="" /></div>
                <div class="titulo">Psicologia</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu,
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>

        <a class="conteudo" href="" title="" data-icon="empresa">
            <div class="item">

                <div class="img"><img src="[template]/pw-images/box-02-05.jpg" alt="" title="" /></div>
                <div class="titulo">Terapia Ocupacional</div>
                <div class="conteudo">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi porta imperdiet arcu,
                </div>
                <div class="entrar">Saiba Mais</div>

            </div>
        </a>

    </div>

</div>

<div class="box-03-total">

    <div class="box-03">

        <div class="linha">
            <div class="barra"></div>
        </div>
        <div class="titulo">ENTRE EM <br />CONTATO</div>

        <form action="mail-contato.php" method="post">
            <div class="item">
                <input name="campo[Nome]" type="text" placeholder="Nome:" />
                <input name="campo[E-mail]" type="text" placeholder="E-mail:" />
                <input name="campo[Telefone]" type="text" placeholder="Telefone:" />
            </div>
            <div class="item">
                <textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
            </div>
            <input class="submit" type="submit" value="Enviar" />
        </form>

    </div>

</div>

<div class="mapa">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.540073829127!2d-47.09060648441886!3d-22.893442393151872!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8c62f6c725a6b%3A0xb336aa74be94ee69!2sDUE-Ufficio!5e0!3m2!1spt-BR!2sus!4v1616697495518!5m2!1spt-BR!2sus" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

</div>