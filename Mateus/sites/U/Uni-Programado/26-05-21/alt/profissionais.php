<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
?>

<div class="conteudo-pages">
    <h1>PROFISSIONAIS</h1>

    <div class="boxPage">

        <!-- Control buttons -->
        <div id="myBtnContainer">
            <button class="btn active" onclick="filterSelection('terapeuta-ocupacional')"> Terapeuta Ocupacional </button>
            <button class="btn" onclick="filterSelection('psicologa')"> Psicóloga</button>
            <button class="btn" onclick="filterSelection('fonoaudiologa')"> Fonoaudióloga </button>
            <button class="btn" onclick="filterSelection('pedagoga')"> Pedagoga</button>
            <button class="btn" onclick="filterSelection('fisioterapeuta')"> Fisioterapeuta</button>
        </div>

        <div class="container">

            <div class="col flex profissionais">

                <div class="filterDiv psicologa">

                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/adriana-psicologa.jpg" alt="" title="">
                        </div>
                        <h2>Adriana Bernardes</h2>
                        <p>Psicóloga</p>
                        <a target="_blank" href="[url]/curriculo-Adriana" class="btnCurriculo">Ver Currículo</a>
                    </div>

                </div>


                <div class="filterDiv terapeuta-ocupacional">

                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/aline-to.jpeg" alt="" title="">
                        </div>
                        <h2>Aline Benedito </h2>
                        <p>Terapeuta Ocupacional</p>
                        <a target="_blank" href="[url]/curriculo-Aline" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv terapeuta-ocupacional">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/amanda-terapeuta-ocupacional.jpg" alt="" title="">
                        </div>
                        <h2>Amanda Alonso</h2>
                        <p>Terapeuta Ocupacional</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv fonoaudiologa">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/amarilis-fono.jpeg" alt="" title="">
                        </div>
                        <h2>Amarílis Ribeiro </h2>
                        <p>Fonoaudióloga </p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv psicologa">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/anayara-psico.jpeg" alt="" title="">
                        </div>
                        <h2>Anayara Cândido</h2>
                        <p>Psicóloga</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv fisioterapeuta">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/andressa-fisio.jpeg" alt="" title="">
                        </div>
                        <h2>Andressa Heringer</h2>
                        <p>Fisioterapeuta</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv pedagoga">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/bianca-pedagoga.jpeg" alt="" title="">
                        </div>
                        <h2>Bianca Fernandes</h2>
                        <p>Pedagoga</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv terapeuta-ocupacional">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/carolina-terapeuta-ocupacional.jpg" alt="" title="">
                        </div>
                        <h2>Carolina Magaldi</h2>
                        <p>Terapeuta Ocupacional</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv fonoaudiologa">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/kharina-fonoaudiologa.jpg" alt="" title="">
                        </div>
                        <h2>Kharina Galleazzo</h2>
                        <p>Fonoaudióloga</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv terapeuta-ocupacional">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/michele-to.jpeg" alt="" title="">
                        </div>
                        <h2>Michele Peracini</h2>
                        <p>Terapeuta Ocupacional</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

                <div class="filterDiv fisioterapeuta">
                    <div class="itemBox">
                        <div class="imgitem">
                            <img src="[template]/pw-images/especialidades/nayara-fisio.jpeg" alt="" title="">
                        </div>
                        <h2>Nayara Clara</h2>
                        <p>Fisioterapeuta</p>
                        <a target="_blank" href="#" class="btnCurriculo">Ver Currículo</a>
                    </div>
                </div>

            </div>

        </div>



    </div>

</div>