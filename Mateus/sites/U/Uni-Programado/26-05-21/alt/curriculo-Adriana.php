<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
?>


<div class="conteudo-pages">

<h1>Profissionais</h1>

    <div class="curriculo-total">

        <div class="curriculo">

            <div class="img-curriculo">
                <img src="[template]/pw-images/medicos/ADRIANA PSICÓLOGA.jpg" alt="" title="">
            </div>

            <div class="texto-curriculo">

                <h2>Adriana Bernardes</h2>
                <h3>Psicóloga</h3>

                <p> <b> Sobre: </b> Psicóloga, bacharel pela PUC-Campinas, especialista em Terapia Cognitiva Comportamental pelo Instituto de Controle do Stress – Campinas, formação em Neuropsicologia pela Faculdade de Medicina da Unicamp e Certificação Internacional em Integração Sensorial pela USC-EUA (Módulo I). Com vasta experiência no atendimento clínico, desde 2008 se dedica exclusivamente no aperfeiçoamento e realização do trabalho em equipe multidisciplinar, especialmente com crianças e adolescentes no espectro autista, paralisia cerebral e síndrome de Down. </p>

                <p><b>Pós-Graduação:</b></p>

                <ul>
                    <li>Especialista em Terapia Cognitiva Comportamental pelo Instituto de Controle do Stress – Campinas;</li>
                    <li>Formação em Neuropsicologia pela Faculdade de Medicina da Unicamp;</li>
                    <li>Certificação Internacional em Integração Sensorial pela USC-EUA (Módulo I).</li>
                </ul>

                <p><b>Cursos de Aperfeiçoamento:</b></p>

                <ul>
                    <li>Formação em Inclusão Escolar pela Inspirados pelo Autismo;</li>
                    <li>Módulo I de formação em tratamento à autistas pelo Inspirados pelo autismo;</li>
                    <li>Formação em Programa de Estimulação de Atenção pela Clínica Qualconsoante – Portugal;</li>
                    <li>Estágio em Distúrbios de Aprendizagem na Clínica Qualconsoante – Portugal;</li>
                    <li>Curso de Inclusão Escolar e Alfabetização para Autistas pelo Neurosaber.</li>
                </ul>
                
            </div>

        </div>

    </div>

</div>