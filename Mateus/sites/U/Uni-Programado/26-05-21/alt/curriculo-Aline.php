<?php
$dadosPagina["titulo"]   = "Modelo de Site Padrão, Personalizado, Layout Moderno 1";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"teste\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Modelo de Site Padrão, Personalizado, Layout Moderno 1\" />";
?>


<div class="conteudo-pages">

    <h1>Profissionais</h1>

    <div class="curriculo-total">

        <div class="curriculo dois">

            <div class="img-curriculo dois">
                <img src="[template]/pw-images/medicos/ALINE TO.jpeg" alt="" title="">
            </div>

            <div class="texto-curriculo">

                <h2>ALINE CRISTINA BENEDITO</h2>

                <h3>Terapeuta Ocupacional</h3>

                <p> <b> Sobre: </b> Terapeuta Ocupacional, bacharel pela Pontifícia Universidade Católica de Campinas (PUC-Campinas) em 2015, Pós Graduada em Neuroaprendizagem pela Unopar (2017), Certificação em Integração Sensorial de Ayres pela Universidade do Sul da Califórnia (USC-EUA), Cursando Conceito Bobath – Tratamento Neuroevolutivo pelo Cern, Aprimoramento em Estimulação Precoce pela UFRN (2017), Aprimoramento em Atenção Domiciliar pela UFMA (2016) e Aperfeiçoamento em Transtorno do Espectro Autista pela AEDRE do Hospital das Clínicas em São Paulo (2016). Atuação e ampla experiência no atendimento clínico e reabilitação de crianças com Atraso no Desenvolvimento (Estimulação Precoce), Transtorno do Espectro Autista - TEA, Síndromes e Desordens Neuromotoras, Transtorno do Processamento Sensorial – TPS, Dificuldades de Aprendizagem, TDAH, Dificuldades na Coordenação Motora Grossa e/ou Fina. Treinamento e domínio na aplicação dos testes e instrumentos avaliativos: Questionário de Responsividade Sensorial em crianças com diagnóstico de Autismo - CMRS, Observações Clínicas Estruturadas de Ayres, Bayley III, BOT2, Beery VMI, PEP-R, BACLE, Teste de Percepção Visual – TVPs. Formações e cursos relacionados à Integração Sensorial, Autismo, Práxis, Coordenação Motora Fina, Transtornos de Aprendizagem, Processamento Visual, Desenvolvimento Infantil, Desordens Neuromotoras, entre outros. Ministrou Palestras e Workshops, em Escolas e Universidades, sobre atuação da Terapia Ocupacional e é autora de produções científicas voltadas para a Atenção e Intervenção na Saúde e Bem-estar dos Cuidadores Informais, isto é, Familiares ou Responsáveis. </p>

            </div>

            <div class="curriculo-dois">

                <p><b>Técnicas</b></p>

                <ul>
                    <li>Integração Sensorial de Ayres;</li>
                    <li>Conceito Bobath – Tratamento Neuroevolutivo (Em formação - Módulo 1 Concluído);</li>
                    <li>Estimulação Precoce;</li>
                    <li>Neuroaprendizagem;</li>
                    <li>Atenção Domiciliar;</li>
                    <li>Atenção à Saúde e Bem-estar dos Cuidadores Informais;</li>
                </ul>

                <p><b>Pós-Graduação:</b></p>

                <ul>
                    <li>Certificação em Integração Sensorial pela Universidade do Sul da Califórnia USC/WPS - EUA (2016-2018);</li>
                    <li>Conceito Bobath – Tratamento pelo Cern (2021 / em formação – Módulo 1 Concluído);</li>
                    <li>Pós-graduada em Neuroaprendizagem pela Unopar (2017);</li>
                    <li>Aperfeiçoamento em Estimulação Precoce pela UFRN (2017);</li>
                    <li>Aprimoramento em Atenção Domiciliar pela UFMA (2016).</li>
                </ul>

                <p><b>Graduação:</b> Bacharel em Terapia Ocupacional pela Pontifícia Universidade Católica de Campinas (2015). </p>

                <p><b>Cursos de Aperfeiçoamento:</b></p>

                <ul>
                    <li>Responsividade Sensorial em crianças com diagnóstico de Autismo com Dr. Gustavo Reinoso, Ph.D., OTR/L Ludens Cursos (2021); </li>
                    <li>Formação Básica no Tratamento baseado no Conceito Neuroevolutivo Bobath Cern (cursando /2020 - 2021);</li>
                    <li>Adaptação de atividades escolares para o contexto virtual Evolutio (2020);</li>
                    <li>Estratégias para controle de esfíncter em crianças com transtornos do desenvolvimento Inclusão Eficiente (2020);</li>
                    <li>Motricidade Fina nas desordens do desenvolvimento infantil Equilibre-se (2020)</li>
                    <li>Alterações do desenvolvimento da criança de 0 a 5 anos de idade: detecção dos primeiros sinais na educação infantil. O que fazer? Inclusão Eficiente (2020)</li>
                    <li>Intervenção Avançada para Práxis Ideacional - Possibilidades para Funções Executivas com Teresa Ann May-Benson, ScD, OTR/L, FAOTA GME Cursos (2020)</li>
                    <li>Raciocínio Clínico Avançado e Planejamento de Tratamento para Intervenção na Práxis com Teresa Ann May-Benson, ScD, OTR/L, FAOTA GME Cursos (2020) </li>
                    <li>Fundamentos da Intervenção na Práxis para Crianças com Autismo com Teresa Ann May-Benson, ScD, OTR/L, FAOTA GME Cursos (2020)</li>
                    <li>A Criança com Deficiência Visual Cortical Avaliação e Intervenção na Atualidade - Módulo1 Play Core Kids (2020) </li>
                    <li>Processamento visual cortical e o desenvolvimento das habilidades visuoespaciais GME Cursos (2019) </li>
                    <li>Bateria Bacle - Prática de Avaliação em Dificuldades de Leitura Escrita e Avaliação e Intervenção em Transtornos de Aprendizagem Equipe Uni (2019) </li>
                    <li>Certificação Completa Internacional em Integração Sensorial USC/WPS - EUA (2016 - 2018)</li>
                    <li>Aperfeiçoamento em Estimulação Precoce UFRN (2016 – 2017) </li>
                    <li>Educação Continuada (Aperfeiçoamento) em Transtorno do Espectro Autista Associação para Educação, Esporte, Cultura e Profissionalização da Divisão de Reabilitação do Hospital das Clínicas de São Paulo (2016) </li>
                    <li>Aprimoramento em Atenção Domiciliar UFMA (2016)</li>
                    <li>Integração Sensorial e Autismo Ludens Cursos (2015) </li>
                    <li> Integração sensorial nos Distúrbios de Aprendizagem e Distúrbios Neurológicos da Infância Ludens Cursos (2015) </li>
                    <li>Fundamentos para o desenvolvimento da Motricidade Fina Ludens Cursos (2015) </li>
                </ul>


                <p><b>Experiência Profissional</b></p>

                <ul>
                    <li>Terapeuta Ocupacional na Equipe Uni – 2019 em diante;</li>
                    <li>Terapeuta Ocupacional em Reabiliart: Terapia Ocupacional Domiciliar – 2015 em diante;</li>
                    <li>Terapeuta Ocupacional na Clínica Teixeira – 2017 a 2018;</li>
                    <li>Experiência de Prática Terapêutica Ocupacional Supervisionada na Clínica Ludens – 2015 a 2017;</li>
                </ul>

                <p><b>Atividades Acadêmicas:</b></p>

                <ul>
                    <li>Co Autora do artigo BALLARIN, M. L. G. S.; BENEDITO, A. C.; KRÖN, C. A.; CHRISTOVAM, D. Perfil sociodemográfico e sobrecarga de cuidadores informais
                        de pacientes assistidos em ambulatório de Terapia Ocupacional Caderno de Terapia Ocupacional da UFSCar, São Carlos, v. 24, n. 2, p. 315-321, 2016. https://doi.org/10.4322/0104-4931.ctoAO0607</li>
                    <li>Apresentação como autora do trabalho CUIDADORES INFORMAIS DE IDOSOS: PRODUÇÃO DE CONHECIMENTO NESTE CAMPO no 13º Congresso Nacional de Iniciação Científica CONIC-SEMESP – 2013 (2 horas).;</li>
                    <li>Participação no CBTO 2013: BALLARIN, M. L. G. S.; BENEDITO, A. C.; KRÖN, C. A.; CHRISTOVAM, D. TERAPIA OCUPACIONAL E O COTIDIANO DE CUIDADORES INFORMAIS DE PESSOAS DEPENDENTES. In: XIII Congresso Brasileiro de Terapia Ocupacional, 2013, Florianópolis – SC. Anais do XIII CBTO, 2013.;</li>
                
                </ul>


            </div>

        </div>

    </div>

</div>