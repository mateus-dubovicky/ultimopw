<?php
$dadosPagina["titulo"]   = "Escola Particular Cristã - Competence";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Escola Particular Cristã. Escola de Formação, Colégio Infantil, Escola Particular, Escola Infantil, Colégio Cristão. Escola Digital, Educação Infantil. \" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Escola Particular Cristã - Competence\" />";
?>

<div class="conteudo-pages">
    <h1>QUEM SOMOS</h1>

    <div class="empresa-total">
        <img src="[template]/pw-images/escola-particular-crista.jpg" alt="Escola Particular Cristã" title="Escola Particular Cristã">

        <div class="texto-empresa">
            <div class="item-text">

                <div class="item">
                    <h2><i class="fas fa-angle-double-right"></i> NOSSA HISTÓRIA</h2>
                    <p>Desde a nossa concepção somos uma escola cristã, que se dedica em realizar tudo para a glória de Deus.</p>
                    <p>Assim, a implantação de um centro educacional no Bairro Pirapora, em São Luís/MA partiu do pressuposto de que as crianças dessa localidade poderiam desfrutar de uma educação de qualidade, baseada em princípios cristãos. </p>
                    <p>Firme nesse propósito,  a Sra. Alaudisia Bastos de Oliveira resolveu fundar, no ano 2000,a Sociedade Beneficente, Profissionalizante ElShaday – SCBPROEL,  cujo nome de fantasia era Centro Educacional Comunitário Competence e Jardim de Infância Pequeno Davi, </p>
                    <p>A escola iniciou suas atividades em um prédio alugado situado à Rua Santa Isabel, 3 – Santo Antônio.</p>
                    <p>Todavia percebemos que precisávamos de um ambiente maior para oferecer mais qualidade para nossos alunos.</p>
                    <p>Atualmente, a escola dispõe de um prédio próprio, com 02 (dois) andares e quadra e diversas salas. Tudo planejado para disponibilizar segurança, conforto e  incentivo ao desenvolvimento da criança.</p>
                    <p>Dessa forma, segue sendo  referência neste bairro e circunvizinhança, proporcionando aos alunos melhor aprendizado e consequentemente na satisfação dos pais e responsáveis. </p>
                </div>

            </div>
        </div>
    </div>

    <div class="mvv-total">
        <div class="mvv">

            <div class="item">
                <div class="icone"><i class="fas fa-location-arrow"></i></div>
                <h3>Missão</h3>
                <p>Alcançar a excelência no ensino da educação infantil e na propagação do evangelho de Cristo, transmitindo ao aluno a capacidade de se destacar, seja na área acadêmica, profissional ou social.</p>
            </div>

            <div class="item">
                <div class="icone"><i class="fas fa-eye"></i></div>
                <h3>Visão</h3>
                <p>Fornecer ao aluno as ferramentas acadêmicas necessárias para se sobressair nos estudos e para habilitá-lo a conhecer as virtudes, os valores e os princípios, a fim de que se torne líder moral e intelectual. </p>
            </div>

            <div class="item">
                <div class="icone"><i class="fas fa-handshake"></i></div>
                <h3>Valores</h3>
                <p>Buscar excelência no ensino de nossos alunos, incutindo amor pelo aprendizado.</p>
                <p>Manter o compromisso de servir à comunidade local, por meio da propagação da Palavra de Deus.</p>
            </div>

        </div>
    </div>

</div>