<?php
$dadosPagina["titulo"]   = "Escola Cristã - Métodos Pedagógicos";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Escola Cristã. Escola de Formação, Colégio Infantil, Instituto de Ensino, Escola Particular, Escola Infantil, Colégio Cristão. Escola Digital, Educação Infantil.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Escola Cristã - Métodos Pedagógicos\" />";
?>

<div class="conteudo-pages">
    <h1>MÉTODO PEDAGÓGICO</h1>
</div>

<div class="page">
    <div class="row single">
        <div class="box-txt">
            <p>Adotamos o método construtivista que valoriza o ser humano, favorece a reconstrução do conhecimento de forma contextualizada em que alunos e professores são pesquisadores de informações e dados que vão permitir a compreensão para construir um mundo melhor, baseada nos cinco pilares de sustentação do aprender, que são: </p>
            <ol>
                <li>Aprender a conhecer</li>
                <li>Aprender a fazer</li>
                <li>Aprender a conviver</li>
                <li>Aprender a ser</li>
                <li>Aprender a aprender</li>
            </ol>
            <p>A metodologia é direcionada para um sistema centrado no aluno, utilizando técnicas de dinâmica de grupo para avaliação do ser humano. </p>
        </div>
    </div>
</div>