<?php


$dadosPagina["titulo"]   = "Escola Infantil em São Luis";
$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Escola Infantil em São Luis, Escola de Formação, Colégio Infantil, Instituto de Ensino, Escola Particular, Escola Infantil, Colégio Cristão. Escola Digital, Educação Infantil.\" />";
$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Escola Infantil em São Luis\" />";


?>

<div class="conteudo-pages">
    <h1>MODALIDADES DE ENSINO</h1>
</div>

<section class="page">
    <div class="row single">
        <div class="box-txt">
            <h2><strong>Modalidades de Ensino:</strong> Educação infantil e Ensino fundamental I</h2> <br><br>
            <h2><strong>Educação Infantil:</strong></h2>
            <ul>
                <li>Maternal – 1 ano e 2 meses até 3 anos</li>
                <li>Infantil I – 4 anos</li>
                <li>Infantil II – 5 anos </li>
            </ul>
            <br><br>
            
            <h2><strong>Ensino Fundamental I:</strong></h2>
            <ul>
                <li>1º Ano – 6 anos </li>
                <li>2º Ano – 7 anos </li>
                <li>3º Ano – 8 anos </li>
                <li>4º Ano – 9 anos</li>
                <li>5º Ano – 10 anos</li>
            </ul>
        </div>
    </div>
</section>