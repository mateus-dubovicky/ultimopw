<?php
	$dadosPagina["titulo"]   = "Distribuidora de Tintas para Demarcação Viária";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Tintas de Demarcação Viária, com Alta Durabilidade, Resistência a Abrasão do Tráfego e Intemperismo com Estabilidade da Cor para Pintura de Ruas e Vias, Atendendo as Normas da ABNT 13699 e ABNT 13731.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Distribuidora de Tintas para Demarcação Viária\" />";
  $dadosPagina["css"] = "";
?>

<div class="conteudo-pages">

	<div class="texto-pages">

    <h2>Tintas</h2>

  		<h3><i class="fas fa-arrow-circle-right"></i> Tinta Demarcação Viária</h3>
  		<div class="produtos-total">
  			<div class="produto-desc">
  				<p>Alpha vias é uma tinta de tecnologia inovadora especialmente desenvolvida para tintas de demarcação viária.</p>
  				<p>Apresenta secagem em câmera a 90% de úmidade, superior a concorrência com resistência a tráfego intenso, ótima resistência à abrasão, ótima pulverização na pistola, excelente ancoragem de microsferas e de retrorrefletância.</p>
  				<p>Trata-se de uma tinta eficiente para atender as norma ABNT NBR 13.699 e Infraero ABNT NBR 13.732 em todos os quesitos.</p>
  				<br>
  				<b>Embalagem: 18 litros</b>
  				<br>
  				<b>Cores: Amarelo demarcação, branco, vermelho, azul e preto</b>
  			</div>
  			<div class="produtos-img">
  				<img src="[template]/pw-images/produtos/tinta-demarcacao-viaria.jpg" alt="Tinta Demarcação Viária" title="Tinta Demarcação Viária" />
  			</div>
  		</div>

	</div><!-- Texto Pages -->

</div> <!-- Conteudo Pages -->
