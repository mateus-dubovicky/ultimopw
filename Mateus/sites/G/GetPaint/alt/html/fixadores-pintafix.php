<?php
	$dadosPagina["titulo"]   = "Fixadores Pintafix";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Fixador para Pintura à Base de Cal ou Aplicáveis em Todos os Tipos de Caiações em Paredes para Acelerar o Processo de Fixação.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Fixadores Pintafix\" />";
  $dadosPagina["css"] = "";
?>

<div class="conteudo-pages">

	<div class="texto-pages">

    <h2>Produtos para construção</h2>

    <h3><i class="fas fa-arrow-circle-right"></i> Pintafix</h3>

      <div class="produtos-total">
        <div class="produto-desc">
          <p>Fixador para pintura à base de cal ou aplicáveis em todos os tipos de caiações em paredes, acelera o processo de fixação.</p>
					<p>Embalagens com 48 unidades.</p>
        </div>
        <div class="produtos-img">
          <img src="[template]/pw-images/produtos/pintafix.jpg" alt="Fixadores Pintafix" title="Fixadores Pintafix">
        </div>
      </div>

	</div><!-- Texto Pages -->

</div> <!-- Conteudo Pages -->
