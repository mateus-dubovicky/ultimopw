<?php
	$dadosPagina["titulo"]   = "Get Paint Industria e Comércio de Tintas";
	$dadosPagina["metas"][0] = "<meta name=\"description\" content=\"Empresa de São Paulo Distribuidora de Produtos para Construção, Pigmentos Industriais, Tintas, Impermeabilizantes, Resinas, Umectantes, Aditivos e Agentes de Reticulação.\" />";
	$dadosPagina["metas"][1] = "<meta name=\"title\" content=\"Get Paint Industria e Comércio de Tintas\" />";
  $dadosPagina["css"] = "";
?>

<div class="slogan-total">

	<div class="slogan">

		<div class="texto">
			SEJA BEM-VINDO
		</div> <!-- texto -->

		<div class="botao">
			CONHEÇA A NOSSA EMPRESA
		</div> <!-- botao -->

	</div> <!-- slogan -->

</div> <!-- slogan total -->


<div class="box-01-total">

	<div class="box-01">

		<div class="titulo-total">

			<div class="titulo">

				<div class="barra">
					<div class="linha"></div>
				</div> <!-- barra -->

				<div class="texto">
					NOSSOS PRODUTOS
				</div> <!-- texto -->

			</div> <!-- titulo -->

		</div> <!-- titulo total -->


		<div class="conteudo">

			<div class="item">
			<a href="[url]/pigmentos-em-po-para-construcao" title="Pigmentos em Po para Construção">
				<div class="nome">
					<div class="titulo">Pigmentos Industriais</div> <!-- titulo -->
					<div class="botao"><i class="fas fa-chevron-right"></i></div> <!-- botao -->
				</div> <!-- nome -->
				<div class="img">
					<img src="[template]/pw-images/index/pigmentos-industriais.jpg" alt="Pigmentos Industriais" title="Pigmentos Industriais"/>
				</div> <!-- img -->
			</a>
			</div> <!-- item -->

			<div class="item">
			<a href="[url]/pigmentos-em-po-alphacolor" title="Pigmentos em Pó Alphacolor">
				<div class="nome">
					<div class="titulo">Pigmentos em Pó</div> <!-- titulo -->
					<div class="botao"><i class="fas fa-chevron-right"></i></div> <!-- botao -->
				</div> <!-- nome -->
				<div class="img">
					<img src="[template]/pw-images/index/pigmento-em-po.jpg" alt="Pigmento em Pó" title="Pigmento em Pó"/>
				</div> <!-- img -->
			</a>
			</div> <!-- item -->

			<div class="item">
			<a href="[url]/fixadores-pintafix" title="Fixadores Pintafix">
				<div class="nome">
					<div class="titulo">Pintafix</div> <!-- titulo -->
					<div class="botao"><i class="fas fa-chevron-right"></i></div> <!-- botao -->
				</div> <!-- nome -->
				<div class="img">
					<img src="[template]/pw-images/index/pintafix.jpg"  alt="Fixadores Pintafix" title="Fixadores Pintafix"/>
				</div> <!-- img -->
			</a>
			</div> <!-- item -->

			<div class="item">
			<a href="[url]/distribuidora-de-tintas-para-demarcacao-viaria" title="Distribuidora de Tintas para Demarcação Viária">
				<div class="nome">
					<div class="titulo">Tintas</div> <!-- titulo -->
					<div class="botao"><i class="fas fa-chevron-right"></i></div> <!-- botao -->
				</div> <!-- nome -->
				<div class="img">
					<img src="[template]/pw-images/index/tinta-demarcacao-viaria.jpg" alt="Tinta Demarcação Viária" title="Tinta Demarcação Viária"/>
				</div> <!-- img -->
			</a>
			</div> <!-- item -->

			<div class="item">
			<a href="[url]/resinas-em-sao-paulo" title="Resinas em São Paulo">
				<div class="nome">
					<div class="titulo">Resinas</div> <!-- titulo -->
					<div class="botao"><i class="fas fa-chevron-right"></i></div> <!-- botao -->
				</div> <!-- nome -->
				<div class="img">
					<img src="[template]/pw-images/index/polimeros.jpg" alt="Polimeros em São Paulo" title="Polimeros em São Paulo"/>
				</div> <!-- img -->
			</a>
			</div> <!-- item -->

			<div class="item">
			<a href="[url]/aditivos-em-sao-paulo" title="Aditivos em São Paulo">
				<div class="nome">
					<div class="titulo">Aditivos</div> <!-- titulo -->
					<div class="botao"><i class="fas fa-chevron-right"></i></div> <!-- botao -->
				</div> <!-- nome -->
				<div class="img">
					<img src="[template]/pw-images/index/umectantes.jpg" alt="Umectantes em São Paulo" title="Umectantes em São Paulo"/>
				</div> <!-- img -->
			</a>
			</div> <!-- item -->

		</div> <!-- conteudo -->

	</div> <!-- box 01 -->

</div> <!-- box 01 total -->

<div class="box-03-total">

	<div class="box-03">

		<div class="titulo-total">

			<div class="titulo">
				PORQUE NOS ESCOLHER
			</div> <!-- titulo -->

			<div class="barra">
				<div class="linha"></div> <!-- linha -->
			</div> <!-- barra -->

		</div> <!-- titulo total -->

		<div class="mvv-total">
			<div class="mvv">
				<div class="mvv-img">
					<img src="[template]/pw-images/sobre/missao.png" alt="Get Paint Distribuidora em São Paulo de Polimeros" title="Get Paint Distribuidora em São Paulo de Polimeros" />
				</div>
				<div class="mvv-titulo">
					<h3>MISSÃO</h3>
				</div>
				<div class="mvv-desc">
					<p>Proporcionar produtos de alta qualidade, desenvolver e comercializar soluções para garantir a segurança das pessoas</p>
				</div>
			</div>

			<div class="mvv">
				<div class="mvv-img">
					<img src="[template]/pw-images/sobre/visao.png" alt="Get Paint Distribuidora em São Paulo de Polimeros" title="Get Paint Distribuidora em São Paulo de Polimeros" />
				</div>
				<div class="mvv-titulo">
					<h3>VISÃO</h3>
				</div>
				<div class="mvv-desc">
					<p>Ser uma empresa de alta tecnologia na fabricação de tintas, produtos e serviços de sinalização viária do Brasil</p>
				</div>
			</div>

			<div class="mvv">
				<div class="mvv-img">
					<img src="[template]/pw-images/sobre/valores.png" alt="Get Paint Distribuidora em São Paulo de Polimeros" title="Get Paint Distribuidora em São Paulo de Polimeros" />
				</div>
				<div class="mvv-titulo">
					<h3>VALORES</H3>
				</div>
				<div class="mvv-desc">
					<p>A Get Paint tem como valores a honestidade, ética e a responsabilidade.</p>
					<p>Atendendo a todos os requisitos legais e normas aplicáveis, objetivando atingir um elevado padrão de desempenho</p>
				</div>
			</div>
		</div>

	</div> <!-- box 03 -->

</div> <!-- box 03 total -->


<div class="box-03-total">

	<div class="box-03">

		<div class="titulo-total">

			<div class="titulo">
				POLÍTICA DA QUALIDADE, SEGURANÇA E MEIO AMBIENTE
			</div> <!-- titulo -->

			<div class="barra">
				<div class="linha"></div> <!-- linha -->
			</div> <!-- barra -->

			<div class="box-03-desc">
				<p>A Get Paint tem como diretrizes básicas satisfazer as necessidade dos clientes fornecendo produtos inovadores e de qualidade para garantir a segurança das pessoas e de revestimento decorativo e de proteção que agreguem valor aos seus negócios, comprometidos com:</p>
			</div>

		</div> <!-- titulo total -->

			<div class="conteudo">

				<div class="container">

					<div class="item">
					<a href="">
						<div class="img"><i class="fas fa-seedling"></i></div> <!-- img -->
						<div class="texto">
							O desenvolvimento sustentável
						</div> <!-- texto -->
					</a>
					</div> <!-- item -->

					<div class="item">
					<a href="">
						<div class="img"><i class="fas fa-user-shield"></i></div> <!-- img -->
						<div class="texto">
							A Saúde e segurança dos colaboradores
						</div> <!-- texto -->
					</a>
					</div> <!-- item -->

					<div class="item">
					<a href="">
						<div class="img"><i class="far fa-smile"></i></div> <!-- img -->
						<div class="texto">
							A Melhoria contínua
						</div> <!-- texto -->
					</a>
					</div> <!-- item -->

					<div class="item">
					<a href="">
						<div class="img"><i class="fab fa-cloudscale"></i></div> <!-- img -->
						<div class="texto">
							A prevenção a poluição e de acidentes e incidentes
						</div> <!-- texto -->
					</a>
					</div> <!-- item -->

					<div class="item">
					<a href="">
						<div class="img"><i class="fas fa-check"></i></div> <!-- img -->
						<div class="texto">
							Atendimento a legislação e aos demais requisitos aplicáveis
						</div> <!-- texto -->
					</a>
					</div> <!-- item -->

				</div> <!-- container -->

			</div> <!-- conteudo -->

	</div> <!-- box 03 -->

</div> <!-- box 03 total -->



<div class="box-04-total">

	<div class="box-04">

		<div class="conteudo">

			<div class="titulo-total">

			  <div class="barra">
					<div class="linha"></div> <!-- linha -->
			  </div> <!-- barra -->
				<div class="titulo">PORTFÓLIO</div> <!-- titulo -->

			</div> <!-- titulo total -->


			<a href="">
				<div class="botao">VEJA TODOS</div> <!-- botao -->
			</a>

		</div> <!-- conteudo -->

	</div> <!-- box 04 -->

	<div class="img">
		<img src="[template]/pw-images/produtos-para-pintura-de-vias.jpg" alt="Produtos para Pintura de Vias" title="Produtos para Pintura de Vias"/>
	</div> <!-- img -->
</div> <!-- box 04 total -->



<div class="mapa">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.643906840045!2d-46.330067384477886!3d-23.437252462907043!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce7d014bcd1235%3A0x989880e7eb4fd78d!2sR.+Mica%2C+155+-+Jardim+Nascente%2C+Itaquaquecetuba+-+SP%2C+08586-420!5e0!3m2!1spt-BR!2sbr!4v1565203390354!5m2!1spt-BR!2sbr" allowfullscreen></iframe>
</div> <!-- mapa -->



<div class="formulario">


    <form action="pw-form.php" method="post">
		<div class="info">
			<input name="campo[Nome]" placeholder="Nome:" type="text" />
			<input name="campo[Telefone]" placeholder="Telefone:" type="text"/>
		</div> <!-- info -->
		<input name="campo[E-mail]" placeholder="E-mail:" type="text"/>
		<textarea name="campo[Mensagem]" placeholder="Mensagem:"></textarea>
		<input class="submit" value="Enviar" type="submit"/>

	</form>

</div> <!-- formulario -->
